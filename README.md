Group name: Game of Codes
Members: 
•	Hania Reda
•	Nada Farag
•	Taymour Abdelal


Requirements Analysis

Main Menu:

-Will be contained within a separate window with a distinct background
•	Start Game button
•	High Score button
•	Exit button
Additional options we intend on adding,
-	A Store button, where you exchange a certain amount of coins for an upgraded weapon
-	Help button, where the player reads about the tactics needed to be employed in order to win the game
-	Option of enabling or disabling sound

The Actual Game:

-Will be contained within a separate window with a distinct background and in the window, you will find:

•	The player’s score (where it will always start with a score of 0 at the beginning of each game)
•	The player’s lives (a default of 3 lives are provided at the beginning of each game)
•	The player in the form of a spaceship
•	Shots being fired from the spaceship
•	Chickens (with their different movements)
•	Eggs being dropped from the same position of the chickens, and at certain timings
•	Asteroids

-Changes within the actual game:

•	Eggs turning to sunny side up eggs when they reach the end of the screen
•	Chickens turning to nuggets once they are shot
•	Score increasing incase of the player shooting a chicken, or collecting a nugget

-How the player loses a life:

•	Collide with egg
•	Collide with chicken
•	Collide with asteroid
In case of a multi-player mode, collision with the other spaceship, or collision with the shots fired from the other spaceship.

-Pickups (dropped randomly throughout the game):

•	Extra lives
•	The nuggets would allow for extra points to the player’s score
•	An option to increase the power of the shots fired from the spaceship
•	coins

-Sound effects:

•	Sound every time a shot is fired
•	Sound every time a chicken is shot at
•	General background music

As soon as the player finish all waves within all three levels, or the player loses all their lives, a different window appears, displaying:
•	The player’s score
•	An overall scoreboard showing the highest scores 
•	An option to return to the main menu



The Menu
•	As soon as the player opens the game, they will find a menu similar to the diagram shown above. The different options will be drawn as texts on the window, and the window will have a background image. 
•	The texts will act as buttons and the player will be capable of pressing these buttons through functions that handle the menu events, and functions that make these texts pressable, attained from the menu class.
The Actual Game
•	When the player presses on the start button, the game class will now be used. This opens a new window, containing the score (initially set at zero), and the player’s lives (initially set at three lives). There will also be a background image, and background music. 
•	We intend on providing a video of our own giving a storyline to the game in a manner similar to all the famous videogames, where this video will play before the game window opens, and has a skip option for the player to immediately start playing
•	Through the use of the player class, the player’s spaceship will be drawn on the window. The handle events function of the game will allow for the movement of this spaceship. For example, if the ‘u’ key is pressed then, player.move(0,-speed), meaning the player will move upwards with the speed we have specified. 
•	In case of the space button being pressed, in expectations of shots being fired from the spaceship, 

-	This would require to set the position of each shot to the very top position of the spaceship (which can be obtained by getting the x-position of the spaceship to set the x-position of the bullet, and for the y-position of the bullet, we add the x-position to the size of the spaceship in the y direction). This process will be done each time the space button is pressed. This would require a vector containing the bullets and their positions.
-	A loop with an iterator will be applied on this vector, and will be used to draw the bullets, using the function the shots class, and to fire them from the spaceship with a specified speed.

•	In the first level’s first wave, through the usage of the chicken class, a chicken will be drawn on the window. The movement of the chicken will also be set through a function from the chicken class that will alter the sprite’s position in the x-direction, to give it, its swaying like motion. 
•	When there are chickens, eggs must fall downwards from the same position as the chickens (where the position of the chicken is obtained from the getpos function in the class chicken). These eggs are to fall at certain timings within a wave, and with determined speeds.
•	To win a wave, it will depend on the wave. If it includes chickens, the player can only win the wave by killing all the chickens. If it includes rocks, it will be time based, where the player can only win by having more than one life by the end of the wave. 
•	The 2nd level’s first wave will include a shower of rocks, this will be implemented through drawing the rocks from the class astroid, where the motion will also be determined with functions from the class. 

In the case of:
o	A collision between the shot and the chicken, the player’s score increases
o	A collision between the player’s spaceship and a nugget, the player’s score increases by a greater amount
o	A collision between the player’s spaceship and a chicken, a life is reduced 
o	A collision between the player’s spaceship and an egg, a life is reduced
o	A collision between the player’s spaceship and a rock (asteroid), a life is reduced
o	If a multi-player mode is implemented in our game, a collision between the players’ spaceships will lead to the reduction of one life from each player
o	A collision between a player’s spaceship and the shots coming from the other spaceship, the first player loses a life
o	A collision between the player’s spaceship and an extra life pickup, a life is added
o	A collision between the player’s spaceship and a pickup that increases the power of the shots, the shots will be fired at a greater speed 
o	A collision between the player’s spaceship and a coin, money will be added to the player’s account


•	Special sound effects will also take place if the detection of the collision is true, where the shooting of the bullets will have a sound, and the correct shooting of a chicken will have another sound 
•	When the eggs falling from the chicken reach a certain position on the window, almost near the end, the eggs turn to sunny side up eggs, which involves a change in sprites (only done when the if condition is met)
•	When the chicken collides with a shot, it changes to a nugget, which involves a change in sprites (only done when the if condition is met)
•	Waves within different levels will vary by adjusting the size of the chicken sprite, the number of the chicken sprites, the shots required to kill the beast, and the speed by which the rocks are falling.
When it comes to killing the beast, a count must be introduced, where it will only increment once a collision between the shot and the beast takes place. If, for example, the count reaches 5, then the beast has been defeated.



















Classes:

#pragma once
#ifndef game_h
#define game_h
#include <SFML/Graphics.hpp>
#include<SFML/Audio.hpp>

class game {

private:
	int lives, score, chickens;
	sf::Text score, lives;
	sf::Texture texture1, texture2, textrue3, textrue4, background; 
	sf::Window game; 
	sf::Music music;
	



public:
	game();
	void setchichkens(int);
	int getchickens();
	void killchicken(int);
	void setmusic(std:: String);
	void setbuttonssound(sf::Sound);
	void setbackground(sf::Texture);
	void setlives(int, Text);
	void setscore(int, Text);
	void setgame(float, float);
	void handleevents();      // both keypress and mouse 
	void setpickup(sf::Texture);
	void setpickup2(sf::Texture);
	void setpickup3(sf::Texture);
	void setpickup4(sf::Texture);
	void exit();
	bool alive();
	void setmusic();
	void createpickup();
	void createpickup2(); 
	void createpickup3(); 
	void createpickup4();
	int getlives();
	int getscore;
	sf::Text drawlives();
	sf::Text drawscore();
	void playmusic();

	
};
#endif
#pragma once


#pragma once

#ifndef menu_h
#define menu_h
#include <String>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include<SFML/Window.hpp>

class menu

{
private:
	int actualsize;
	sf::Texture texture;
	sf::Window menu;
	sf::Window endScreen;
	sf::Text text1, text2, text3, text4, text5;
	sf::Texture buttonsMain[100];
	sf::Music music;
	sf::Sound buttons;
	




public:
	menu();
	endmenu();
	
	void setmusic(std::String);
	void setbuttonsound(std::String);
	void setbackgroundmenu(Texture);
	void setbackgroundendscreen(Texture);
	void setmenu(float, float);
	void setendscreen(float, float);
	void drawmainmenu(sf::RenderWindow);
	void drawendscreen(sf::RenderWindow );
	void setText(sf::Text,sf::Text,sf::Text,sf::Text,sf::Text);
	sf::Text drawText1();
	sf::Text drawText2();
	sf::Text drawText3();
	sf::Text drawText4();
	void quit();
	void handleeventsmenu();
	void handleeventsendscreen();
	void playmusic();
	void playbuttonsound();

};
#endif

#ifndef chicken_hpp
#define chicken_hpp
#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
int const size=50;
using namespace sf;

class chicken
{
private:
    Texture texture;
    Sprite chick;
    Sprite eggs[size];
    Vector2f pos;
    Vector2f size;
    
public:
    chicken();
    void drawchicken(sf::RenderWindow &,Sprite);
    void movechicken() const;
    void eggmovement();
    void setsize(Vector2f);
    Vector2f getsize() const;
    void setpos(Vector2f);
    Vector2f getpos() const;
    bool hitchicken();
};


#endif /* chicken_hpp */

#ifndef astroid_hpp
#define astroid_hpp
#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
using namespace sf;

class astroid
{
private:
    Texture texture;
    Sprite astr;
    Vector2f pos;
    Vector2f size;
    
public:
    astroid();
    void drawastroid(sf::RenderWindow &,Sprite);
    void moveastroid(Sprite) const;
    void setsize(Vector2f);
    Vector2f getsize(Sprite) const;
    void setpos(Vector2f);
    Vector2f getpos(Sprite) const;
};


#endif

#ifndef player_h
#define player_h

class player
{
private: 
	sf::texture playertexture;
	sf::sprite playersprite; 
	float size;
	float positionx;
	float positiony;

public:
void setsize(sf::vector2f s);
player(); //default constructor
player (sf::texture &playertexture, sf::sprite &playersprite)
{
	playertexture.loadfromfile(“pictureofspaceship”); //Upload a picture as texture
	playersprite.setTexture(playertexture);
playersprite.setSize(size);
}
void drawplayer(sf::renderwindow &window)
{
	window.draw(playersprite);
}
	void getsize();

	void setpositionx();
	void setpositiony();

	void getpositionx();
	void getpositiony();
	
void playermovement(char key, float speed)
{	switch(key)
	{
		case ‘u’:
		playersprite.move(0, -speed);
		break;
		case ‘d’:
		playersprite.move(0,speed);
		break;
		case ‘l’:
		playersprite.move(-speed,0);
		break;
		case ‘r’:
		playersprite.move(speed,0);
		break;
		}
	}
}
#endif

#ifndef shots_h
#define shots_h
class shots
{
	private:
	sf::texture shotstexture;
	sf::sprite shotsprite;
float size;
float bottomposition;
float topposition;
float rightposition;
float leftposition;
	
	public:

	void setsize(sf::vector2f s);

shots(); //default constructor
	shots(sf::texture &shotstexture, sf::sprite& shotsprite)
	{
		shotstexture.loadfromfile(“imageofmissile”);
		shotsprite.setTexture(shotstexture);
		shotsprite.setSize(size);
	}
	void shoot (float speed)
	{
		shotsprite.move(speed,0);
	}
float setrightposition();
	float setleftposition();
	float settopposition();
	float setbottomposition();


	float getrightposition();
	float getleftposition();
	float gettopposition();
	float getbottomposition();

	void drawshots (sf::renderwindow &window);
}
#endif