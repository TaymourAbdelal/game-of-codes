//
//  chicken.cpp
//  chicken
//
//  Created by Hania's air on 4/8/17.
//  Copyright © 2017 Hania's air. All rights reserved.
//

#include "chicken.hpp"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include <iostream>
#include <cmath>
using namespace sf;
chicken:: chicken()
{
    if (!texture1.loadFromFile(resourcePath() + "chick1.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(1, 1);
    
    if (!texture2.loadFromFile(resourcePath() + "chick2.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(1, 1);
    
    if (!texture3.loadFromFile(resourcePath() + "chick3.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(1, 1);
    
    if (!texture4.loadFromFile(resourcePath() + "chick4.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.2, 0.2);
    
}

void chicken::drawchicken(RenderWindow &window)
{
    window.draw(chick);
}

void chicken::movechicken(RenderWindow &window,Clock flap)
{
    
    Time done;
    done+= flap.restart();
    
    while (done> seconds(1))
        done-=seconds(1);
    if (done<=seconds(0.25))
        
        chick.setTexture(texture1);
    
    else if (done<=seconds(0.5))
        
        chick.setTexture(texture2);
    
    else if (done<=seconds(0.75))
        
        chick.setTexture(texture3);
    
    else
        
        chick.setTexture(texture4);
    drawchicken(window);
}
bool chicken::MoveLeft(int speed,RenderWindow &window,double x)
{
    if(chick.getPosition().x+x>=window.getPosition().x)
        chick.move(-speed,0);
    else
        return true;
    
    return false;
}
bool chicken::Move(int speed,RenderWindow &window,double x)
{
    if(chick.getPosition().x+x<window.getSize().x)
        chick.move(speed,0);
    else
        return false;
    
    return true;
}


float chicken::getsize() const
{
    return texture1.getSize().x;
}

Vector2f chicken::getpos() const
{
    return chick.getPosition();
}
void chicken::setpos(float x, float y)
{
    chick.setPosition(x, y);
}
void chicken::beastload()
{
    if (!btex1.loadFromFile(resourcePath() + "beast1up.png"))
    {
        std:: cout<< "Not loaded";
    }
    beast.setScale(0.75, 0.75);
    
    if (!btex2.loadFromFile(resourcePath() + "beast1mid.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.75, 0.75);
    
    if (!btex3.loadFromFile(resourcePath() + "beast1down.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.75, 0.75);
}
void chicken::movebeast(RenderWindow &window, Clock clock)
{
    Time done;
    done+= clock.restart();
    
    while (done> seconds(1))
        done-=seconds(1);
    if (done<=seconds(0.25))
        
        chick.setTexture(btex1);
    
    else if (done<=seconds(0.5))
        
        chick.setTexture(btex2);
    
    else if (done<=seconds(0.75))
        
        chick.setTexture(btex3);
    
    else
        
        chick.setTexture(btex2);
    drawchicken(window);
}
