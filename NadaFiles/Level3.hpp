//
//  Level3.hpp
//  Chicken Invaders
//
//  Created by Nada Ahmed Farag on 5/5/17.
//  Copyright © 2017 Nada Ahmed Farag. All rights reserved.
//
#include "player.h"
#include "shots.h"
#include "Darth.hpp"
#include "eggs.hpp"
#include "pickups.h"
#include "game.h"
#include "chicken.hpp"
#ifndef Level3_hpp
#define Level3_hpp

#include <stdio.h>
class level3
{
private:
public:
    //General game declaration
    int j;
    player p;
    sf::Clock clockExplosion;
    sf::Clock clock, drop2c, drop1c, drop3c, drop4c, wavetxtc;
    std::vector<shots> container; //for bullets
    int Numshots = 1;
    bool firing = false;
    bool playerflag = false;
    pickups p1, p2, p3, p4;
    bool drop3 = false, drop4 = false, drop2 = false, drop1 = false;
    std::vector<pickups> pickup4, pickup3, pickup2, pickup1;
    game game;
    bool flagbeast = true;
    int power = 1;
    int z;
    bool  alive = true;
    int beasthits = 0;
    int sc;
    //-------------------------
    float x = 400;
    bool flag;
    Darth d;
    float angle = 0;
    egg beam;
    std::vector<egg> beamcontainer;
    sf::Music breath;
    bool IsVaderAlive = true;
    //-------------------------------
    //Wave 1 Declarations
    chicken chick;
    egg e;
    std::vector<chicken> c;
    float dx= chick.getpos().x;
    sf::Clock chickenclock;
    sf:: Clock flap,clock1;
    std::vector<egg> r;
     bool flageggs[200];
     int indexeggs[200] = { 0 };
    bool flagchickens[200];
    //------------------------------
    level3();
    int handlevents(sf::RenderWindow &window);
    int beast(sf::RenderWindow &window,class game &g);
    int wave1(sf::RenderWindow &window,class game &g);
};
#endif /* Level3_hpp */
