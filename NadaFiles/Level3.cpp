
#include "Level3.hpp"
#include "player.h"
#include "eggs.hpp"
#include "shots.h"
#include "Darth.hpp"
#include "pickups.h"
#include "chicken.hpp"
#include "game.h"
#include <SFML/graphics.hpp>

level3::level3()
{ breath.openFromFile(resourcePath()+"DarthVaderBreath.mp3");
    beam.beamload();
    beam.seteggpos(d.getpos().x + 100, d.getpos().y + 50);
    for (int i = 0; i < r.size(); i++) //Initial flags
        flagchickens[i] = false;
    
}
int level3::handlevents(sf::RenderWindow &window)
{
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
            {   firing = true;
                
            }
            p.movement(event, window);
        }
        
        window.clear();
    }
}
int level3::beast(sf::RenderWindow &window,class game &g)
{    chick.setpos(window.getPosition().x,0);
    d.moveVader(window, clock);
    if (firing == true)
    {
        float a, b, c;
        c = p.getsize() / 2.0;
        a = p.getpositionx();
        b = p.getpositiony();
        shots newshot(sf::Vector2f(5, 10));
        shots uppedshot(sf::Vector2f(5, 10));
        newshot.setshotposition(sf::Vector2f(a + c, b));
        uppedshot.setshotposition(sf::Vector2f(a + c + 20, b));
        container.push_back(newshot);
        firing = false;
        
    }
    p.drawplayer(window, playerflag, clockExplosion);
    game.drawhud(window);
    //if (power == 1)
    {
        
        
        for (int i = 0; i < container.size(); i++)
        {
            
            
            
            container[i].drawshots(window, Numshots);
            container[i].fire(50);
            
            
            
        }
    }
    if (clock.getElapsedTime().asSeconds() >= 0.50) //To make the sprite rotate
    {
        angle-=0.05;
        
        d.movementrectangle(angle);
        //d.Move(0.6, window,0.05);
    }
    //----------------------------------------------
    if (flag == true) //For movement from left to right
        flag = d.Move(0.5, window, 120);
    
    else
        flag = d.MoveLeft(0.5, window, 50);
    //----------------------------------------------

    if (rand() % 200 == 0) //for dropping beams
        
    {
        beam.seteggpos(d.getpos().x + d.getxsize()/2,d.getpos().y + d.getysize()/2);
        beamcontainer.push_back(beam);

    }
    
    for (int i = 0; i < beamcontainer.size(); i++)
    {   sf::Time drop;
        while (drop>seconds(20))
            drop-= seconds(20);
        if(drop <= seconds(20))
        beamcontainer[i].eggmovement(window, 4);
        
        
    }
    //------------------------------------------
    for (int i = 0; i < beamcontainer.size(); i++)
    {
        beamcontainer[i].drawegg(window);
    }
    //-----------------------------------------------
    /*while (IsVaderAlive)
    {

        breath.play();
    }*/
    //-----------------------------------------
    
    for (int j = 0; j < container.size(); j++)
        
        if (container[j].rect.getGlobalBounds().intersects(d.DarthSprite.getGlobalBounds()))
        {
            
            beasthits++;
            container.erase(container.begin() + j);
        }
    int x = game.getscore();
    if (beasthits < 10)
        d.drawVader(window);
    else
    {
        d.setpos(1000.0, 1000.0);
        x = x + 1000;
        game.setscore(x, window);
        return 2;
        
        
    }
    //----------------------------------------------------------------
    z = game.getlives();
    for (int e = 0; e < beamcontainer.size(); e++)
        if (beamcontainer[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
            
            if (z - 1 > 0)
            {
                z= z - 1;
                playerflag = false;
                
                alive = true;
                game.setlives(z);
                game.drawlives(window);
                p.drawplayer(window, playerflag, clockExplosion);
                beamcontainer.erase(beamcontainer.begin() + e);
                e--;
                
                return 1;
                
                
            }
            else
            {
                playerflag = true;
                game.setlives(0);
                game.drawlives(window);
                p.drawplayer(window, playerflag, clockExplosion);
                return 0;
                
            }
    
    
    

    //----------------------------------------
    //Drop extra life pickup4===============================================================
   
            if (rand() % 1000 == 0)
            {
                
                
                p4.droppp4(d.getpos().x , d.getpos().y , window);
                
                
                
                pickup4.push_back(p4);
            }
            
            for (int i = 0; i < pickup4.size(); i++)
            {
                
                pickup4[i].movepp4();
                
            }
    
    
    
    
    
    if (drop4 == false)
    {
        
        for (int i = 0; i < pickup4.size(); i++)
        {
            pickup4[i].drawpp4(window);
            
        }
    }
    //pickup3========================================================
   
            if (rand() % 500 == 0)
            {
                
                
                p3.droppp3(d.getpos().x , d.getpos().y , window);
                
                
                
                pickup3.push_back(p3);
            }
            
            for (int i = 0; i < pickup3.size(); i++)
            {
                
                pickup3[i].movepp3();
                
            }

    if (drop3 == false)
    {
        for (int i = 0; i < pickup3.size(); i++)
        {
            pickup3[i].drawpp3(window);
            
        }
    }
    
    //pickup2----------------------------------------------------------------------
  

            if (rand() % 500 == 0)
            {
                
                
                p2.droppp2(d.getpos().x , d.getpos().y , window);
                
                
                
                pickup2.push_back(p2);
            }
            
            for (int i = 0; i < pickup2.size(); i++)
            {
                
                pickup2[i].movepp2();
                
            }
    
    
    
    
    if (drop2 == false)
    {
        
        for (int i = 0; i < pickup2.size(); i++)
        {
            pickup2[i].drawpp2(window);
            
        }
    }
    
    //pickup1----------------------------------------------------
            if (rand() % 500 == 0)
            {
                
                
                p1.droppp1(d.getpos().x , d.getpos().y , window);
                
                
                
                pickup1.push_back(p1);
            }
            
            for (int i = 0; i < pickup1.size(); i++)
            {
                
                pickup1[i].movepp1();
                
            }
    
//------------------------------------------------------------------
//-----------------------------------------------------------------
    // collision drop 4===========================================================================
    for (int i = 0; i < pickup4.size(); i++)
    {
        if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
        {
            int x;
            sf::Clock drops;
            
            drop4 = true;
            game.setpickuptxt("Extra Life");
            x = game.getlives();
            x++;
            game.setlives(x);
            game.drawlives(window);
            
            
            if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
            {
                game.pickuptxtclear();
                
                
            }
            drops.restart();
            game.drawpickuptxt(window);
        }
        
    }
    if (drop4c.getElapsedTime().asSeconds() > 10)
    {
        drop4 = false;
        drop4c.restart();
    }
    
    
    // collision drop 3===========================================================================
    for (int i = 0; i < pickup3.size(); i++)
    {
        if (pickup3[i].pp3.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
        {
            sf::Clock drops;
            
            drop3 = true;
            game.setpickuptxt("Laser");
            
            
            if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
            {
                game.pickuptxtclear();
            }
            drops.restart();
            game.drawpickuptxt(window);
        }
        
    }
    if (drop3c.getElapsedTime().asSeconds() > 10)
    {
        drop3 = false;
        drop3c.restart();
    }
    // collision drop 2===========================================================================
    for (int i = 0; i < pickup2.size(); i++)
    {
        if (pickup2[i].pp2.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
        {
            sf::Clock drops;
            
            drop2 = true;
            game.setpickuptxt("PowerUp");
            power = 2;
            if (Numshots < 2)
                Numshots++;
            
            if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
            {
                game.pickuptxtclear();
            }
            drops.restart();
            game.drawpickuptxt(window);
        }
        
    }
    if (drop2c.getElapsedTime().asSeconds() > 10)
    {
        drop2 = false;
        drop2c.restart();
    }
    //Shield Case
    
    // collision drop 1===========================================================================
    for (int i = 0; i < pickup1.size(); i++)
    {
        if (pickup1[i].pp1.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
        {
            sf::Clock drops;
            //std::cout << "1";
            drop1 = true;
            game.setpickuptxt("Shield");
            
            
            if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
            {
                game.pickuptxtclear();
            }
            drops.restart();
            game.drawpickuptxt(window);
        }
        
    }
    d.drawVader(window);
    window.display();
}
//-------------------------
int level3::wave1(sf::RenderWindow &window,class game &g)
{    if (firing == true)
{
    float a, b, c;
    c = p.getsize() / 2.0;
    a = p.getpositionx();
    b = p.getpositiony();
    shots newshot(sf::Vector2f(5, 10));
    shots uppedshot(sf::Vector2f(5, 10));
    newshot.setshotposition(sf::Vector2f(a + c, b));
    uppedshot.setshotposition(sf::Vector2f(a + c + 20, b));
    container.push_back(newshot);
    firing = false;
    
}
    //if (power == 1)
    {
        
        
        for (int i = 0; i < container.size(); i++)
        {
            
            
            
            container[i].drawshots(window, Numshots);
            container[i].fire(50);
            
            
            
        }
    }
    p.drawplayer(window, playerflag, clockExplosion);
    game.drawhud(window);
//------------------------------------------
    if (rand()%1000 ==0)
    {
        float dy=rand()%750-150;
        float zero = 0;
        chick.setpos(dy, zero);
        c.push_back(chick);
    }
    for (int i=0; i<c.size(); i++) //chickenflap
    {
        
        c[i].movechicken(window,chickenclock);
    }
    if (clock1.getElapsedTime().asSeconds()>=0.25)
    {
        for (int i=0; i<c.size(); i++)
        {
            c[i].Movechickdown(10, window);
        }
        clock1.restart();
    }
    for (int i=0; i<r.size(); i++)
        r[i].drawegg(window);
    for (int i=0; i<c.size(); i++)
        c[i].drawchicken(window);
    
//----------------------------------------------
    //Drop extra life pickup4===============================================================
    for (int i = 0; i < r.size(); i++)
   
        {
            if (rand() % 1000 == 0)
            {
                
                
                p4.droppp4(c[i].getpos().x , c[i].getpos().y , window);
                
                
                
                pickup4.push_back(p4);
            }
            
            for (int i = 0; i < pickup4.size(); i++)
            {
                
                pickup4[i].movepp4();
                
            }
        }
    
    
    
    
    if (drop4 == false)
    {
        
        for (int i = 0; i < pickup4.size(); i++)
        {
            pickup4[i].drawpp4(window);
            
        }
    }
    //pickup3========================================================
    for (int i = 0; i < r.size(); i++)
    {
    
            if (rand() % 500 == 0)
            {
                
                
                p3.droppp3(c[i].getpos().x , c[i].getpos().y , window);
                
                
                
                pickup3.push_back(p3);
            }
            
            for (int i = 0; i < pickup3.size(); i++)
            {
                
                pickup3[i].movepp3();
                
            }
    }
    
    if (drop3 == false)
    {
        for (int i = 0; i < pickup3.size(); i++)
        {
            pickup3[i].drawpp3(window);
            
        }
    }
    
    //pickup2----------------------------------------------------------------------
    for (int i=0; i <r.size(); i++)
            if (rand() % 500 == 0)
            {
                
                
                p2.droppp2(c[i].getpos().x , c[i].getpos().y , window);
                
                
                
                pickup2.push_back(p2);
            }
            
            for (int i = 0; i < pickup2.size(); i++)
            {
                
                pickup2[i].movepp2();
                
            }
    
    
    
    
    if (drop2 == false)
    {
        
        for (int i = 0; i < pickup2.size(); i++)
        {
            pickup2[i].drawpp2(window);
            
        }
    }
    
    //pickup1----------------------------------------------------
    for (int i = 0; i < r.size(); i++)

        {
            if (rand() % 500 == 0)
            {
                
                
                p1.droppp1(c[i].getpos().x , c[i].getpos().y , window);
                
                
                
                pickup1.push_back(p1);
            }
            
            for (int i = 0; i < pickup1.size(); i++)
            {
                
                pickup1[i].movepp1();
                
            }
        }
    
    if (drop1 == false)
    {
        for (int i = 0; i < pickup1.size(); i++)
        {
            pickup1[i].drawpp1(window);
            
        }
    }
//-----------------------------------
//Egg Drops
    for (int i = 0; i<r.size(); i++)
        {
            if (rand() % 500 == 0)
                
            {
                e.seteggpos(c[i].getpos().x + 100, c[i].getpos().y + 50);
                r.push_back(e);
            }
            
            for (int i = 0; i<r.size(); i++)
            {
                r[i].eggmovement(window, 0.4);
                r[i].eggchange(window);
                
            }
        }
    for (int i = 0; i<r.size(); i++)
        r[i].drawegg(window);
    for (int i = 0; i < container.size(); i++)
    {
        for (int j = 0; j < r.size(); j++)
        {
            if (container[i].rect.getGlobalBounds().intersects(r[j].eggs.getGlobalBounds()))
            {
                
                
                flageggs[j] = true;
                indexeggs[j]++;
                
            }
        }
    }
    for (int j = 0; j<r.size(); j++)
    {
        if (flageggs[j] == false)
            r[j].drawegg(window);
        if (flageggs[j] == true && indexeggs[j]<2)
        {
            
            r[j].drawegg(window);
        }
        
    }
  //----------------------------------
    for (int x = 0; x < container.size(); x++)
    {
        for (int i = 0; i < r.size(); i++)
        {
            
                if (container[x].rect.getGlobalBounds().intersects(c[i].chick.getGlobalBounds()))
                {
                    std::cout << "!!!!!!!!!!!!!!" << std::endl;
                    flagchickens[i] = true;
                    
                    //c[i][j].setpos(1000, 1000);
                    sc = game.getscore();
                    sc = sc + 100;
                    std::cout << sc;
                    //game.setscore(sc , window);
                    container.erase(container.begin()+x);
                    x--;
                    
                    break;
                    
                }
                
            
        }
    }
    for (int g = 0; g < r.size(); g++)
    {
        if (r[g].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
        {
            playerflag = true;
            alive = false;
            z = game.getlives();
            //cout << z;
            if (Numshots >  1)
                Numshots--;
            r.erase(r.begin() + g);
            g--;
            if (z - 1 > 0)
            {
                z = z - 1;
                playerflag = false;
                
                alive = true;
                game.setlives(z);
                game.drawlives(window);
                p.drawplayer(window, playerflag, clockExplosion);
                //r[g].erase();
                
                
            }
            else
                game.setlives(0);
            game.drawlives(window);
            std::cout << "dead";
        }

    
    game.setscore(sc, window);
    
    }
//---------------------------------------
j = game.getlives();
for (int g = 0; g < r.size(); g++)
    {
        if (r[g].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
                {
                    playerflag = true;
                    alive = false;
                    
                    //cout << z;
                    if (Numshots >  1)
                        Numshots--;
                    r.erase(r.begin() + g);
                    g--;
                    if (j - 1 > 0)
                    {
                        j = j - 1;
                        playerflag = false;
                        
                        alive = true;
                        game.setlives(j);
                        game.drawlives(window);
                        p.drawplayer(window, playerflag, clockExplosion);
                        //r[g].erase();
                        
                        
                        
                        
                    }
                    else
                        return 0;
                    game.setlives(0);
                    game.drawlives(window);
                    
                    
                    
                }
            }
                
    game.setscore(sc, window);
            
//collisiondrop4===========================================================================
                for (int i = 0; i < pickup4.size(); i++)
            {
                if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
                {
                    int x;
                    sf::Clock drops;
                    
                    drop4 = true;
                    game.setpickuptxt("Extra Life");
                    x = game.getlives();
                    x++;
                    game.setlives(x);
                    game.drawlives(window);
                    
                    
                    if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
                    {
                        game.pickuptxtclear();
                        
                        
                    }
                    drops.restart();
                    game.drawpickuptxt(window);
                    }
                
            }
                if (drop4c.getElapsedTime().asSeconds() > 10)
            {
                drop4 = false;
                drop4c.restart();
            }
                
                
                // collision drop 3===========================================================================
            for (int i = 0; i < pickup3.size(); i++)
            {
                if (pickup3[i].pp3.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
                {
                    sf::Clock drops;
                    
                    drop3 = true;
                    game.setpickuptxt("Laser");
                    
                    
                    if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
                    {
                        game.pickuptxtclear();
                    }
                    drops.restart();
                    game.drawpickuptxt(window);
                }
                
            }
    
        if (drop3c.getElapsedTime().asSeconds() > 10)
            {
                drop3 = false;
                drop3c.restart();
            }
                // collision drop 2===========================================================================
                for (int i = 0; i < pickup2.size(); i++)
            {
                if (pickup2[i].pp2.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
                {
                    sf::Clock drops;
                    
                    drop2 = true;
                    game.setpickuptxt("PowerUp");
                    power = 2;
                    if (Numshots < 2)
                        Numshots++;
                    
                    if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
                    {
                        game.pickuptxtclear();
                    }
                    drops.restart();
                    game.drawpickuptxt(window);
                }
                
            }
                if (drop2c.getElapsedTime().asSeconds() > 10)
            {
                drop2 = false;
                drop2c.restart();
            }
                
// collision drop 1===========================================================================
                for (int i = 0; i < pickup1.size(); i++)
            {
                if (pickup1[i].pp1.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
                {
                    sf::Clock drops;
                    //std::cout << "1";
                    drop1 = true;
                    game.setpickuptxt("Shield");
                    
                    
                    if ((drops.getElapsedTime().asSeconds() >= 5.00000) && (drops.getElapsedTime().asSeconds() <= 5.10000))
                    {
                        game.pickuptxtclear();
                    }
                    drops.restart();
                    game.drawpickuptxt(window);
                }
                
    
}
}
