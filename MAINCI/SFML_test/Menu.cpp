#include "menu.h"
#include <iostream>
#include <String>
#include <SFML/Graphics.hpp>
#include <vector>

#include<SFML/Window.hpp>

using namespace std;

menu::menu(float height, float width)
{
	backgroundtxt.loadFromFile("backgroundci.png");
	background.setTexture(backgroundtxt);
	if (!font.loadFromFile("DeathStar.otf"))
	{
		/*handleerror*/
	}

	/*buttonstexture[0].loadFromFile("");
	buttonstexture[1].loadFromFile("");
	buttonstexture[2].loadFromFile("");
	buttonstexture[3].loadFromFile("");
	buttonstexture[4].loadFromFile("");*/
	/*for (int i = 0; i < Max_Number_of_Items; i++)
	{
		buttonsmainmenu[i].setTexture(buttonstexture[i]);
	}
*/
	text[0].setFont(font);
	text[0].setFillColor(sf::Color::White);
	text[0].setOutlineThickness(2);
	text[0].setOutlineColor(sf::Color::White);
	text[0].setString("Play Game");
	text[0].setPosition(sf::Vector2f(width / 2-50, 350));

	text[1].setFont(font);
	text[1].setFillColor(sf::Color::White);
	text[1].setOutlineThickness(2);
	text[1].setOutlineColor(sf::Color::Black);
	text[1].setString("HighScore");
	text[1].setPosition(sf::Vector2f(width / 2 -50, 500));

	text[2].setFont(font);
	text[2].setFillColor(sf::Color::White);
	text[2].setOutlineThickness(2);
	text[2].setOutlineColor(sf::Color::Black);
	text[2].setString("Exit");
	text[2].setPosition(sf::Vector2f(width / 2 -50,  650));

	/*text[3].setFont(font);
	text[3].setFillColor(sf::Color::Black);
	text[3].setOutlineThickness(2);
	text[3].setOutlineColor(sf::Color::Yellow);
	text[3].setString("Options");
	text[3].setPosition(sf::Vector2f(width / 2, height / (Max_Number_of_Items + 1) * 4));

	text[4].setFont(font);
	text[4].setFillColor(sf::Color::Black);
	text[4].setOutlineThickness(2);
	text[4].setOutlineColor(sf::Color::Yellow);
	text[4].setString("Exit");
	text[4].setPosition(sf::Vector2f(width / 2, height / (Max_Number_of_Items + 1) * 5));*/
	score[0].setFont(font);
	score[0].setFillColor(sf::Color::White);
	score[0].setPosition(sf::Vector2f(400, 200));
	score[0].setString("Your Score");
	
	score[1].setFont(font);
	score[1].setFillColor(sf::Color::White);
	score[1].setPosition(sf::Vector2f(400, 300));
	
	
	

	score[3].setFont(font);
	score[3].setFillColor(sf::Color::White);
	score[3].setPosition(sf::Vector2f(400, 400));
	
	
	victorytxt[0].setFont(font);
	victorytxt[0].setFillColor(sf::Color::White);
	victorytxt[0].setPosition(sf::Vector2f(400, 200));
	victorytxt[0].setString("VICOTRYYYYY");

	victorytxt[1].setFont(font);
	victorytxt[1].setFillColor(sf::Color::White);
	victorytxt[1].setPosition(sf::Vector2f(400, 300));
	victorytxt[1].setString("Your Score is");

	victorytxt[3].setFont(font);
	victorytxt[3].setFillColor(sf::Color::White);
	victorytxt[3].setPosition(sf::Vector2f(400, 400));
	victorytxt[3].setString("Play Again");


	highscoremenu[0].setFont(font);
	highscoremenu[0].setFillColor(sf::Color::White);
	highscoremenu[0].setPosition(sf::Vector2f(400, 200));
	highscoremenu[0].setString("HighScore");

	highscoremenu[1].setFont(font);
	highscoremenu[1].setFillColor(sf::Color::White);
	highscoremenu[1].setPosition(sf::Vector2f(400, 300));

	highscoremenu[2].setFont(font);
	highscoremenu[2].setFillColor(sf::Color::White);
	highscoremenu[2].setPosition(sf::Vector2f(400, 400));

	highscoremenu[3].setFont(font);
	highscoremenu[3].setFillColor(sf::Color::White);
	highscoremenu[3].setPosition(sf::Vector2f(400, 500));

	highscoremenu[4].setFont(font);
	highscoremenu[4].setFillColor(sf::Color::White);
	highscoremenu[4].setPosition(sf::Vector2f(400, 600));

	
	
	
	backgroundmenu.loadFromFile("space-cat-wallpaper-2.jpg");
	backgroundmenusprite.setTexture(backgroundmenu);
	backgroundmenusprite.setPosition(0, 0);
	backgroundmenusprite.setScale(2, 2);
	if (!texture.loadFromFile("Chicken_Invaders_4_Background_Space.jpg"))
	{
		//error
	}
}
void menu::drawmainmenu(sf::RenderWindow &window)
{
	window.draw(background);
	for (int i = 0; i < Max_Number_of_Items; i++)
	{
		window.draw(text[i]);
		window.draw(buttonsmainmenu[i]);
	}
}
void menu::moveup()
{
	if (selecteditemindex - 1 >= 0)
	{
		text[selecteditemindex].setOutlineColor(sf::Color::Black);
		selecteditemindex--;
		text[selecteditemindex].setOutlineColor(sf::Color::White);
	
	}
}

void menu::movedown()
{
	if (selecteditemindex + 1 < Max_Number_of_Items)
	{
		text[selecteditemindex].setOutlineColor(sf::Color::Black);
		selecteditemindex++;
		text[selecteditemindex].setOutlineColor(sf::Color::White);
	}
}

void menu::setbackgroundmenu(sf::Window &window)
{
	
	background.setPosition(sf::Vector2f(0, 0));
	
}
int menu::mainmenuloop(menu &menu, sf::RenderWindow& window , std::vector<int> score)
{
	
	while (window.isOpen())
	{
		
		l = menu.handleevents(window, menu);
		
		window.clear();
		menu.setbackgroundmenu(window);
		menu.drawmainmenu(window);
		
		if (l == 0)
		{
			return l;
		}
		
	
		
		if (l == 1)
		{
			menu.drawhighscore(score);
		}
		window.display();
		
		
	}
	
}
int menu::handleevents(sf::RenderWindow & window , menu& menu)
{ 
	int switchreturn = 10;
	sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
					
				case::sf::Keyboard::Up:
				{
					
					menu.moveup();
					return 12;
					break;
				}
				case::sf::Keyboard::Down:
				{
					
					menu.movedown();
					return 11;
					break;
				}
				case::sf::Keyboard::Return:
					switch (menu.getpressed())
					{
					case 0:
					{
						
						switchreturn = 0;
						return switchreturn;

						break;
					}
					case 1:
						return 1;
						break;
					case 2:
						window.close();
						break;
					}
					return 15;
					break;

				}
				return 14;
				break;
			case sf::Event::Closed:
				window.close();
				return 13;
				break;
			}
			

		}
		return 16;
	}
void menu::endmenu(sf::RenderWindow & window, game & game)
{
	int x = game.getscore();
	score[1].setString(to_string(x));

	window.draw(backgroundmenusprite);
	window.draw(score[0]);
	window.draw(score[1]);

}
int menu::endscreenloop(sf::RenderWindow & window, game & game  , menu & menu)
{
	int l = 10;
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{

			case sf::Event::Closed:
				window.close();
			}
		}
		 l =menu.endscreenhandleevents(window, menu);
		window.clear();
			menu.endmenu(window, game);
		window.display();
		if (l == 0)
			return 0;
		

	}
	return 7;
}
int menu::endscreenhandleevents(sf::RenderWindow& window, menu &  menu)
{
	

	sf::Event event;

	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed:
			switch (event.key.code)
			{

			case::sf::Keyboard::Return:
			{

				return 0;
			}
			case sf::Event::Closed:
				window.close();
			}
		}
	}
}
void menu::victorymenu(sf::RenderWindow & window, game & game )
{
	int x = game.getscore();
	victorytxt[2].setString(to_string(x));

	window.draw(background);
	window.draw(victorytxt[0]);
	window.draw(victorytxt[1]);
	window.draw(victorytxt[2]);
	window.draw(victorytxt[3]);
	window.draw(victorytxt[4]);
	window.draw(victorytxt[5]);

}

int menu::victorymenuloop(sf::RenderWindow & window, game & game, menu & menu)
{
	int l = 10;
	while (window.isOpen())
	{
		/*l = menu.endscreenhandleevents(window, menu);*/
		window.clear();
		menu.victorymenu(window, game);
		window.display();
		//	if (l == 0)
		//	return 0;
	}
	return 7;
}

void menu::sethighscore(std::vector < int> score)
{
	int x[4];

	for (int i = 0; i < 3; i++)
	{
		x[i] = score[i];
		highscoremenu[i+1].setString(to_string(x[i]));
		
	}
	
}

void menu::drawhighscore( std :: vector<int> score)

{
	sf::RenderWindow window (sf::VideoMode(800, 800) , "HighScore");
	sethighscore(score);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
				
				case sf::Event::Closed:
					window.close();
				}
			}
		
		window.clear();
		window.draw(backgroundmenusprite);
		for (int i = 0; i < 5; i++)
		{
			window.draw(highscoremenu[i]);
		}

		window.display();
	}
}
