//
//  chicken.hpp
//  chicken
//
//  Created by Hania's air on 4/8/17.
//  Copyright © 2017 Hania's air. All rights reserved.
//

#ifndef chicken_hpp
#define chicken_hpp
#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>


int const size=5;
using namespace sf;

class chicken
{
private:
    Texture texture1;
    Texture texture2;
    Texture texture3;
    Texture texture4;
    
    Texture btex1;
    Texture btex2;
    Texture btex3;
    Texture shiptex;
    Vector2f pos;
    Vector2f size;
    
public:
    Sprite chick;
    chicken();
    bool Move(int speed,RenderWindow &window, double);
    bool MoveLeft(int speed,RenderWindow &window,double);
    void drawchicken(sf::RenderWindow &);
    //void drawbeast(RenderWindow &window);
    Sprite getsprite() const;
    void movechicken(RenderWindow &,Clock);
    void setsize(Vector2f);
    float getsize() const;
    void setpos(float,float);
    Vector2f getpos() const;
    bool hitchicken();
    void beastload();
    void movebeast(RenderWindow &, Clock);
    void beast2load();
    void beast2move();
    bool Moveup(int speed,RenderWindow &window, double);
    bool Movedown(int speed,RenderWindow &window, double x);

};


#endif /* chicken_hpp */
