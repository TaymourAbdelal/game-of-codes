//
//  level2.cpp
//  Chicken Invaders
//
//  Created by Nada Ahmed Farag on 5/4/17.
//  Copyright © 2017 Nada Ahmed Farag. All rights reserved.
//

#include "level2.hpp"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include <vector>
#include "player.h"
#include "shots.h"
#include "rocks.h"
#include "game.h"
#include <time.h>
#include <iostream>
level2::level2()
{
    for (int m =0; m <container.size();m++)
        flagbullets[m] = false;
    if (!font.loadFromFile("pdark.ttf"))
    {
        /*handleerror*/
    }
	wavetxt.setFont(font);
    wavetxt.setFillColor(sf::Color::White);
    
    wavetxt.setPosition(sf::Vector2f(100, 100));
	spaceshipb.setpos(50, -500);
	spaceshipb.beast2load();
      spaceshipb.beast2move();
	    
	     laser.laserload();
    
}
//------------------
int level2::wave1(sf::RenderWindow& window, game & game)
{


	game.drawbackground(window);
	game.drawscore(window);
	game.drawlives(window);
	game.drawhud(window);
	if (firing == true)
	{
		float a, b, c;
		c = p.getsize() / 2.0;
		a = p.getpositionx();
		b = p.getpositiony();
		shots newshot(sf::Vector2f(5, 10));
		newshot.setshotposition(sf::Vector2f(a + c, b));
		container.push_back(newshot);
		firing = false;

	}

	for (int i = 0; i < container.size(); i++)
	{


		container[i].drawshots(window, 1);
		container[i].fire(100);

	}
	p.drawplayer(window, playerflag, clockExplosion);
	if ((rand() % 100 == 0) && (counter++ < 10))
	{   //float a = rand()% 50;
		//a = a/10;
		float y = rand() % 700 - 400;
		r.setposition(y);
		//r.scalerocks(a, a);
		rockcontainer.push_back(r);
		flagrocks.push_back(0);


	}
	if (c.getElapsedTime().asSeconds() >= 0.5)
	{

		for (int n = 0; n < rockcontainer.size(); n++)
		{

			rockcontainer[n].move(window);
		}

		c.restart();
	}
	//for (int j = 0; j < rockcontainer.size(); j++)
	//{
	//	if (rockcontainer[j].getposition() >= window.getSize().x)
	//	{
	//		rockcounter++;
	//		std::cout << rockcounter << std::endl;
	//	}
	//}

	for (int j = 0; j < rockcontainer.size(); j++)
		if (flagrocks[j] == 2) {
			rockcontainer.erase(rockcontainer.begin() + j);
			flagrocks.erase(flagrocks.begin() + j);
			j--;
		}
		else if (flagrocks[j] > 0)
			flagrocks[j]++;
	int w;
	w = game.getscore();

	for (int i = 0; i < container.size(); i++)
	{
		bool flag = true;
		for (int j = 0; j < rockcontainer.size() && flag; j++)

		{
			if (container[i].rect.getGlobalBounds().intersects(rockcontainer[j].sprite.getGlobalBounds()) && flagrocks[j] == 0)
			{

				flagrocks[j] = 1;
				//flagbullets[i] = true;
				container.erase(container.begin() + i);

				i--;
				w = w + 100;
				game.setscore(w, window);
				rockcounter++;
				//indexrocks[j]++;

				//
				// if(indexrocks[j]>=2)
				{

					//rockcontainer.erase(rockcontainer.begin()+ j);
					// j--;
					flag = false;

				}


			}
		}

	}

	for (int j = 0; j < rockcontainer.size(); j++)
	{

		if (flagrocks[j] == 1)
			rockcontainer[j].changetopooftexture(false);
		rockcontainer[j].draw(window);


	}

	int x;


	x = game.getlives();
	for (int g = 0; g < rockcontainer.size(); g++)
		if (rockcontainer[g].sprite.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()) && flagrocks[g] == 0
			)
		{
			playerflag = true;

			if (x - 1 > 0)
			{
				playerflag = false;

				x--;
				game.setlives(x);
				game.drawlives(window);
				p.drawplayer(window, playerflag, clockExplosion);
				rockcontainer.erase(rockcontainer.begin() + g);
				flagrocks.erase(flagrocks.begin() + g);
				g--;
			}
			else
			{
				game.setlives(0);
				game.drawlives(window);
				return 0;

			}
		}

			if ((counter >= 10))
			{
				boss = beast2(window, game);
				if (boss == 3)
					return 3;
			}


			

			game.setwavetxt("Wave2 YEEEE BOIIII");
			if ((clocktxt.getElapsedTime().asSeconds() >= 6.00000))
			{
				game.waveclear();

			}
			game.drawwavetxt(window);
			int t = game.getlives();
			if (t == 0)
			{
				return 0;
			}
			return 2;
		}

int level2::gameloop(sf::RenderWindow &window, game & game)
{

	/* window.create(sf::VideoMode(800, 800), "Game");*/
	
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
				window.close();
			}
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
			{
				firing = true;

			}
			p.movement(event, window);

		}

		window.clear();
		result = wave1(window, game);

		window.display();
		if ((result == 3))
			return 3;
		if (result == 0)
			return 0;

		return 2;
	}




int level2::beast2(sf::RenderWindow& window, game &game)
{



if (firing == true)
{
	float a, b, c;
	c = p.getsize() / 2.0;
	a = p.getpositionx();
	b = p.getpositiony();
	shots newshot(sf::Vector2f(5, 10));
	newshot.setshotposition(sf::Vector2f(a + c, b));
	container.push_back(newshot);
	firing = false;

}

for (int i = 0; i < container.size(); i++)
{


	container[i].drawshots(window, 1);
	container[i].fire(100);

}
p.drawplayer(window, playerflag, clockExplosion);

if (clockspace.getElapsedTime().asSeconds() >= 0.25)
{
	if (flagspaceup == true)
		flagspaceup = spaceshipb.Movedown(10, window, 650);

	clockspace.restart();

}

if (flagspaceup == true)
{
	if (rand() % 10 == 0)

	{
		laser.seteggpos(spaceshipb.getpos().x + 100, spaceshipb.getpos().y + 260);
		laser1.push_back(laser);
	}

	for (int i = 0; i<laser1.size(); i++)
	{
		laser1[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
	if (rand() % 10 == 0)

	{
		laser.seteggpos(spaceshipb.getpos().x + 475, spaceshipb.getpos().y + 260);
		laser2.push_back(laser);
	}

	for (int i = 0; i<laser2.size(); i++)
	{
		laser2[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
}
//if(clockspace2.getElapsedTime().asSeconds()>=0.25)
if (flagspaceup == false)
{
	//for (int x = 0; x < laser1.size(); x++)
	//{
	//	laser1.erase(laser1.begin() + x);
	//	x--;
	//}
	//for (int x = 0; x < laser2.size(); x++)
	//{
	//	laser2.erase(laser2.begin() + x);
	//	x--;
	//}
	if (rand() % 100 == 0)

	{
		eggs.seteggpos(spaceshipb.getpos().x + 475, spaceshipb.getpos().y + 190);
		egg2.push_back(eggs);
	}

	for (int i = 0; i<egg2.size(); i++)
	{
		egg2[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
	if (rand() % 100 == 0)

	{
		eggs.seteggpos(spaceshipb.getpos().x + 230, spaceshipb.getpos().y + 190);
		egg1.push_back(eggs);
	}

	for (int i = 0; i<egg1.size(); i++)
	{
		egg1[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
	if (rand() % 100 == 0)

	{
		eggs.seteggpos(spaceshipb.getpos().x + 230, spaceshipb.getpos().y + 370);
		egg3.push_back(eggs);
	}

	for (int i = 0; i<egg3.size(); i++)
	{
		egg3[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
	if (rand() % 100 == 0)

	{
		eggs.seteggpos(spaceshipb.getpos().x + 470, spaceshipb.getpos().y + 370);
		egg4.push_back(eggs);
	}

	for (int i = 0; i<egg4.size(); i++)
	{
		egg4[i].eggmovement(window, 1);
		//eggcont[i].eggchange(window);

	}
	j = game.getlives();
	//==============beast collisions =======================================


	if (spaceshipb.chick.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
	{
		forcefield = false;
		p.playersprite.setPosition(400, 600);
		playerflag = true;
		alive = false;

		//cout << z;
		if (Numshots >  1)
			Numshots--;

		if (j - 1 > 0)
		{
			j--;
			playerflag = false;

			alive = true;
			game.setlives(j);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			//r[g].erase();




		}
		else
		{
			game.setlives(0);
			game.drawlives(window);
			return 0;
		}

	}




}

spaceshipb.drawchicken(window);
for (int i = 0; i<laser1.size(); i++)
	laser1[i].drawegg(window);
for (int i = 0; i<laser2.size(); i++)
	laser2[i].drawegg(window);
for (int i = 0; i<egg1.size(); i++)
	egg1[i].drawegg(window);
for (int i = 0; i<egg2.size(); i++)
	egg2[i].drawegg(window);
for (int i = 0; i<egg3.size(); i++)
	egg3[i].drawegg(window);
for (int i = 0; i<egg4.size(); i++)
	egg4[i].drawegg(window);

game.drawhud(window);
//spaceshipb.drawchicken(window);
for (int j = 0; j < container.size(); j++)
	if (container[j].rect.getGlobalBounds().intersects(spaceshipb.chick.getGlobalBounds()))
	{
		//std::cout << "yass";
		beasthits++;



		container.erase(container.begin() + j);

		j--;


	}

int x = 0;
x = game.getscore();

if (beasthits < 20)
	spaceshipb.drawchicken(window);
else
{
	
	spaceshipb.setpos(1000, 1000);
	x = x + 1000;
	game.setscore(x, window);
	
	return 3;


}
//---------------------------------------------------------------------------------------------Laser1Collision

z = game.getlives();
for (int e = 0; e < laser1.size(); e++)
	if (laser1[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			laser1.erase(laser1.begin() + e);
			e--;




		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}
//--------------------------------------------------------------- Laser2Collision
for (int e = 0; e < laser2.size(); e++)
	if (laser2[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			laser2.erase(laser2.begin() + e);
			e--;




		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}
//--------------------------------------------------------Egg1Collision
for (int e = 0; e < egg1.size(); e++)
	if (egg1[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			egg1.erase(egg1.begin() + e);
			e--;




		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}
//--------------------------------------------------------Egg2Collision
for (int e = 0; e < egg2.size(); e++)
	if (egg2[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			egg2.erase(egg2.begin() + e);
			e--;




		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}
//--------------------------------------------------------Egg3Collision
for (int e = 0; e < egg3.size(); e++)
	if (egg3[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			egg3.erase(egg3.begin() + e);
			e--;




		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}
//--------------------------------------------------------Egg4Collision
for (int e = 0; e < egg4.size(); e++)
	if (egg4[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

		if (z - 1 > 0)
		{
			z = z - 1;
			playerflag = false;

			alive = true;
			game.setlives(z);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);
			egg4.erase(egg4.begin() + e);
			e--;

		}
		else
		{
			playerflag = true;
			game.setlives(0);
			game.drawlives(window);
			p.drawplayer(window, playerflag, clockExplosion);

			return 0;

		}


	int t = game.getlives();
	if (t == 0)
	{
		return 0;
	}
	

}






		

	
	
		
	












