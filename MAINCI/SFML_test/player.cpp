#include <SFML/graphics.hpp>
#include <SFML/System.hpp>
//#include "ResourcePath.hpp"
#include "player.h"
#include "shots.h"
#include <iostream>

player::player()
{
    playertexture.loadFromFile("mainspaceship.png");
    extremeright.loadFromFile("Right4.png");
    extemeleft.loadFromFile("left1.png");
    playersprite.setTexture(playertexture);
    playersprite.setPosition( 400,400);
	if (!blast.openFromFile(("shotsound.ogx")))
		std::cout << "Not Loaded";

}
player::player(sf::Texture T,std::string string)
{
    T.loadFromFile(string);
    playersprite.setTexture(T);
}
//---------------------------------------------
float player::getsize()
{
    return playertexture.getSize().x;
}
//---------------------------------------------

float player::getpositionx() const
{
    return playersprite.getPosition().x;
}
//-----------------------------------------
float player::getpositiony() const
{
    return playersprite.getPosition().y;
}
//------------------------------------------
void player::drawplayer(sf::RenderWindow &window, bool b, sf::Clock clockExplosion)
{
	if (b == false)
		window.draw(playersprite);
	else
		explosion(window, clockExplosion);
}
//----------------------------------------
void player::movement(sf::Event &event,sf::RenderWindow &window)
{   bool space = false;
float px, py;
px = playersprite.getPosition().x;
py = playersprite.getPosition().y;
if ((px > 0) && (px < window.getSize().x - 100) && (py < window.getSize().y - 100) && (py > 0))
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Down:
		{
			/*if ((py < window.getSize().y - 200) && (py > 0))
			{*/
			playersprite.move(0, 60);
			/*	}
				else if (py >= window.getSize().y - 200)
				{
					playersprite.move(0, -10);
				}*/
		}
		break;
		case sf::Keyboard::Up:
		{
			/*if (py > 0 )
			{*/
			playersprite.move(0, -60);
			/*}
			else if (py <= 0)
			{
				playersprite.move(0, 10);
			}*/
		}
		break;
		case sf::Keyboard::Right:

		{
			/*if ((px > 0) && (px < window.getSize().x - 100))
			{*/
		/*	playersprite.setTexture(extremeright);*/
			playersprite.move(60, 0);
			/*}


			else if (px >= window.getSize().x - 200)
			{
				playersprite.move(-10, 0);
			}*/
		}

		break;
		case sf::Keyboard::Left:
		{/*
			if ((px > 0) && (px < window.getSize().x - 100))
			{*/
			/*playersprite.setTexture(extemeleft);*/

			playersprite.move(-60, 0);
			/*}
			else if (px <= 0)
			{
				playersprite.move(10, 0);

			}*/

		}

		break;
		case sf::Keyboard::Space:
		{
			blast.play();
			/*playersprite.move(0, 5);*/
			
			
		}
		break;
		}
	/*case sf::Event::KeyReleased:
		switch (event.key.code)
		{

		case sf::Keyboard::Left:
			playersprite.setTexture(playertexture);
			break;

		case sf::Keyboard::Right:
			playersprite.setTexture(playertexture);
			break;*/


		
	}
}
else if (px <= 0)
playersprite.move(10, 0);
else if (px >= window.getSize().x - 200)
{
	playersprite.move(-10, 0);
}
else if (py >= window.getSize().y - 200)
{
	playersprite.move(0, -10);
}
else if (py <= 0)
{
	playersprite.move(0, 10);
}

    
                    
                
                /*window.clear();*/
            /*    window.draw(playersprite);*/
         /*       window.display();*/
    
    
    
        
}

void player::explosion(sf::RenderWindow &window, sf::Clock clock)

{
	texture1.loadFromFile( "e1.png");
	texture2.loadFromFile( "e2.png");
	texture3.loadFromFile( "e3.png");
	texture4.loadFromFile( "e4.png");
	texture5.loadFromFile("e5.png");
	texture6.loadFromFile( "e6.png");
	texture7.loadFromFile( "e7.png");
	texture8.loadFromFile( "e8.png");
	texture9.loadFromFile( "e9.png");
	sf::Time interval;
	interval += clock.restart();

	while (interval> sf::seconds(2.50))
		interval -= sf::seconds(2.50);
	if (interval <= sf::seconds(0.25))
	{
		explosionsprite.setTexture(texture1);
	}
	else if (interval <= sf::seconds(0.5))
	{
		explosionsprite.setTexture(texture2);
	}
	else if (interval <= sf::seconds(0.75))
	{
		explosionsprite.setTexture(texture3);
	}
	else if (interval <= sf::seconds(1.00))
	{
		explosionsprite.setTexture(texture4);
	}
	else if (interval <= sf::seconds(1.25))
	{
		explosionsprite.setTexture(texture5);
	}
	else if (interval <= sf::seconds(1.50))
	{
		explosionsprite.setTexture(texture6);
	}
	else if (interval <= sf::seconds(1.75))
	{
		explosionsprite.setTexture(texture7);
	}
	else if (interval <= sf::seconds(2.00))
	{
		explosionsprite.setTexture(texture8);
	}
	else if (interval <= sf::seconds(2.25))
	{
		explosionsprite.setTexture(texture9);

	}
	else if (interval <= sf::seconds(2.50))
	{
		explosionsprite.setColor(sf::Color::Transparent);
	}
	
	explosionsprite.setPosition(getpositionx(), getpositiony());
	window.draw(explosionsprite);


}
#include <SFML/graphics.hpp>
#include "shots.h"
#ifndef rocks_h
#define rocks_h
class rocks
{
private:
	int speed;
	sf::Vector2f direction;
	sf::Clock clock;

public:
	sf::Sprite sprite;
	sf::Texture texture;
	sf::Texture pooftexture;
	rocks();
	rocks(const rocks &r);
	void setposition(float &y);
	void scalerocks(float &x, float &y);
	void draw(sf::RenderWindow &window);
	bool isrockstillthere();
	void move(sf::RenderWindow &window);
	void fillcontainer();
	void collision(shots s);
	void changetopooftexture(bool b);



};

#endif /* rocks_h */

//-----------------------------
//fns that gets the position and applies the shooting and calls shot movement
