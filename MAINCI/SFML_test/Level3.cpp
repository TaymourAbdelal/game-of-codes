
#include "Level3.hpp"
#include "player.h"
#include "eggs.hpp"
#include "shots.h"
#include "Darth.hpp"
#include "pickups.h"
#include "chicken.hpp"
#include "game.h"
#include <SFML/graphics.hpp>
#include <cmath>

level3::level3()
{ /*breath.openFromFile("DarthVaderBreath.mp3");*/
    beam.beamload();
    beam.seteggpos(d.getpos().x + 100, d.getpos().y + 50);
    for (int i = 0; i < r.size(); i++) //Initialize flags
        flagchickens[i] = false;
	/*chick.setpos(-500, -500);*/
    
}
void level3::handlevents(sf::RenderWindow &window)
{
            sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
            {   firing = true;
                
            }
            p.movement(event, window);
        }
        
        
    
}
int level3::beast(sf::RenderWindow &window, game &game)
{
	bool DidDarthland;
	DidDarthland = d.Movedown(10, window, 300);
    d.moveVader(window, clock);
	d.Movedown(0.5, window, 300);
    if (firing == true)
    {
        float a, b, c;
        c = p.getsize() / 2.0;
        a = p.getpositionx();
        b = p.getpositiony();
        shots newshot(sf::Vector2f(5, 10));
        shots uppedshot(sf::Vector2f(5, 10));
        newshot.setshotposition(sf::Vector2f(a + c, b));
        uppedshot.setshotposition(sf::Vector2f(a + c + 20, b));
        container.push_back(newshot);
        firing = false;
        
    }
    p.drawplayer(window, playerflag, clockExplosion);
    game.drawhud(window);
    //if (power == 1)
    {
        
        
        for (int i = 0; i < container.size(); i++)
        {
            
            
            
            container[i].drawshots(window, Numshots);
            container[i].fire(100);
            
            
            
        }
    }
    if (clock.getElapsedTime().asSeconds() >= 0.50) //To make the sprite rotate
    {
        angle-=0.05;
        
        d.movementrectangle(angle);
		
		if ((flag == true) && (!DidDarthland)) //For movement from left to right
			flag = d.Move(10, window, 120);

		else
			flag = d.MoveLeft (10, window, 50);
        //d.Move(0.6, window,0.05);
    }
    //----------------------------------------------
    //if (flag == true) //For movement from left to right
    //    flag = d.Move(0.5, window, 120);
    //
    //else
    //    flag = d.MoveLeft(0.5, window, 50);
    //----------------------------------------------

    if (rand() % 200 == 0) //for dropping beams
        
    {
        beam.seteggpos(d.getpos().x + d.getxsize()/2,d.getpos().y + d.getysize()/2);
        beamcontainer.push_back(beam);

    }
    
    for (int i = 0; i < beamcontainer.size(); i++)
    {   sf::Time drop;
        while (drop>seconds(20))
            drop-= seconds(20);
        if(drop <= seconds(20))
        beamcontainer[i].eggmovement(window, 20);
        
        
    }
    //------------------------------------------
    for (int i = 0; i < beamcontainer.size(); i++)
    {
        beamcontainer[i].drawegg(window);
    }
    //-----------------------------------------------
    /*while (IsVaderAlive)
    {

        breath.play();
    }*/
    //-----------------------------------------
    
    for (int j = 0; j < container.size(); j++)
        
        if (container[j].rect.getGlobalBounds().intersects(d.DarthSprite.getGlobalBounds()))
        {
            
            beasthits++;
            container.erase(container.begin() + j);
        }
    int x = game.getscore();
    if (beasthits < 30)
        d.drawVader(window);
    else
    {
        d.setpos(1000.0, 1000.0);
        x = x + 1000;
        game.setscore(x, window);
        return 4;
        
        
    }
    //----------------------------------------------------------------
    z = game.getlives();
    for (int e = 0; e < beamcontainer.size(); e++)
        if (beamcontainer[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
            
            if (z - 1 > 0)
            {
                z= z - 1;
                playerflag = false;
                
                alive = true;
                game.setlives(z);
                game.drawlives(window);
                p.drawplayer(window, playerflag, clockExplosion);
                beamcontainer.erase(beamcontainer.begin() + e);
                e--;
                
                return 3;
                
                
            }
            else
            {
                playerflag = true;
                game.setlives(0);
                game.drawlives(window);
                p.drawplayer(window, playerflag, clockExplosion);

                return 0;
                
            }
    
    
    

    //----------------------------------------
    //Drop extra life pickup4===============================================================
   
            if (rand() % 1000 == 0)
            {
                
                
                p4.droppp4(d.getpos().x , d.getpos().y , window);
                
                
                
                pickup4.push_back(p4);
            }
            
            for (int i = 0; i < pickup4.size(); i++)
            {
                
                pickup4[i].movepp4();
                
            }
    
    
    
    
    
    if (drop4 == false)
    {
        
        for (int i = 0; i < pickup4.size(); i++)
        {
            pickup4[i].drawpp4(window);
            
        }
    }

    // collision drop 4===========================================================================
			int shield = 0;
			for (int i = 0; i < pickup4.size(); i++)
			{
				if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
				{
					//int x;
					//

					//drop4 = true;
					//
					if (pickup4[i].type == 2)
					{
						x = game.getlives();
						x++;
						game.setlives(x);
						game.drawlives(window);


						//
						//game.drawpickuptxt(window);
					}
					else if (pickup4[i].type == 1)
					{
						drop2 = true;
						game.setpickuptxt("PowerUp");
						power = 2;
						if (Numshots < 2)
							Numshots++;



						//game.drawpickuptxt(window);
					}
					else if (pickup4[i].type == 0)
					{
						drop1 = true;
						forcefield = true;
						game.setpickuptxt("Shield");

						shield = game.getlives();
						shield = shield + 1;
						game.setlives(shield);
					}
					game.drawpickuptxt(window);
					pickup4.erase(pickup4.begin() + i);
					i--;
				}

			}
			if (forcefield == true)
				p1.drawforcefield(window, p);

    d.drawVader(window);


	int t = game.getlives();
	if (t == 0)
	{
		return 0;
	}
	return 3;
}
//-------------------------
int level3::wave1(sf::RenderWindow &window, game &game)
{
	int x;
	x = game.getscore();

	game.drawbackground(window);

	game.drawscore(window);
	game.drawlives(window);
	game.setwavetxt(" Wave 2 Dont Look Down");
	if ((clocktxt.getElapsedTime().asSeconds() >= 6.00000))
	{
		game.waveclear();
	}
	game.drawwavetxt(window);
	if (firing == true)
	{

		float a, b, c;
		c = p.getsize() / 2.0;
		a = p.getpositionx();
		b = p.getpositiony();
		shots newshot(sf::Vector2f(5, 10));
		shots uppedshot(sf::Vector2f(5, 10));
		newshot.setshotposition(sf::Vector2f(a + c, b));
		uppedshot.setshotposition(sf::Vector2f(a + c + 20, b));
		container.push_back(newshot);
		firing = false;

	}
	/* if (power == 1)*/
	{


		for (int i = 0; i < container.size(); i++)
		{



			container[i].drawshots(window, Numshots);
			container[i].fire(100);



		}
	}
	p.drawplayer(window, playerflag, clockExplosion);
	game.drawhud(window);

	//------------------------------------------
	if (chickenno < 20)
	{
		if (rand() % 50 == 0)
		{
			float dy = rand() % 750 - 150;
			float zero = 0;
			chick.setpos(dy, zero);
			c.push_back(chick);
			chickenno++;

		}
	}
	
	
	for (int i = 0; i < c.size(); i++) //chickenflap
	{

		c[i].movechicken(window, chickenclock);
	}
	if (clock1.getElapsedTime().asSeconds() >= 0.25)
	{
		for (int i = 0; i < c.size(); i++)
		{
			c[i].Movedown(20, window, -800);
		}
		clock1.restart();
	}
	for (int i = 0; i < r.size(); i++)
		r[i].drawegg(window);
	for (int i = 0; i < c.size(); i++)
		if (flagchickens[i] == false)
		{
			c[i].drawchicken(window);
		}
	//----------------------------------------------
		//Drop extra life pickup4===============================================================


	for (int i = 0; i < pickup4.size(); i++)
	{

		pickup4[i].movepp4();

	}





	if (drop4 == false)
	{

		for (int i = 0; i < pickup4.size(); i++)
		{
			pickup4[i].drawpp4(window);

		}
	}

	//-----------------------------------
	//Egg Drops
	for (int i = 0; i < r.size(); i++)
	{
		if (rand() % 10 == 0)

		{
			e.seteggpos(c[i].getpos().x + 100, c[i].getpos().y + 50);
			r.push_back(e);
		}


		r[i].eggmovement(window, 0.4);
		//r[i].eggchange(window);




	}

	/*for (int i = 0; i < container.size(); i++)
	{
		for (int j = 0; j < r.size(); j++)
		{
			if (container[i].rect.getGlobalBounds().intersects(r[j].eggs.getGlobalBounds()))
			{


				flageggs[j] = true;
				indexeggs[j]++;

			}
		}
	}*/
	/*for (int j = 0; j < r.size(); j++)
	{
		if (flageggs[j] == false)
			r[j].drawegg(window);
		if (flageggs[j] == true && indexeggs[j] < 2)
		{

			r[j].drawegg(window);
		}

	}*/
	//----------------------------------
	sc = game.getscore();
	for (int x = 0; x < container.size(); x++)
	{
		for (int i = 0; i < c.size(); i++)
		{

			if (container[x].rect.getGlobalBounds().intersects(c[i].chick.getGlobalBounds()))
			{
				
					
					
					flagchickens[i] = true;

					//c[i][j].setpos(1000, 1000);
					
					sc = sc + 100;
			
					game.setscore(sc , window);
					container.erase(container.begin() + x);
					x--;
					for (int i = 0; i < r.size(); i++)

					if (rand() % 100 == 0)
					{


						p4.droppp4(c[i].getpos().x, c[i].getpos().y, window);



						pickup4.push_back(p4);
					}
					break;

				}


			}
		}
		for (int g = 0; g < r.size(); g++)
		{
			if (r[g].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
			{
				playerflag = true;
				alive = false;
				z = game.getlives();
				//cout << z;
				if (Numshots > 1)
					Numshots--;
				r.erase(r.begin() + g);
				g--;
				if (z - 1 > 0)
				{
					z = z - 1;
					playerflag = false;

					alive = true;
					game.setlives(z);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					//r[g].erase();


				}
				else
				{
					game.setlives(0);
					game.drawlives(window);
					return 0;

					
				}
			}


			game.setscore(sc, window);

		}
		//---------------------------------------
		j = game.getlives();
		for (int g = 0; g < r.size(); g++)
		{
			if (r[g].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
			{
				playerflag = true;
				alive = false;

				//cout << z;
				if (Numshots > 1)
					Numshots--;
				r.erase(r.begin() + g);
				g--;
				if (j - 1 > 0)
				{
					j = j - 1;
					playerflag = false;

					alive = true;
					game.setlives(j);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					//r[g].erase();




				}
				else
				{
					return 0;
					game.setlives(0);
					game.drawlives(window);
				}


			}
		}



		//collisiondrop4===========================================================================
		int shield = 0;
		for (int i = 0; i < pickup4.size(); i++)
		{
			if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
			{
				//int x;
				//

				//drop4 = true;
				//
				if (pickup4[i].type == 2)
				{
					x = game.getlives();
					x++;
					game.setlives(x);
					game.drawlives(window);


					//
					//game.drawpickuptxt(window);
				}
				else if (pickup4[i].type == 1)
				{
					drop2 = true;
					game.setpickuptxt("PowerUp");
					power = 2;
					if (Numshots < 2)
						Numshots++;



					//game.drawpickuptxt(window);
				}
				else if (pickup4[i].type == 0)
				{
					drop1 = true;
					forcefield = true;
					game.setpickuptxt("Shield");

					shield = game.getlives();
					shield = shield + 1;
					game.setlives(shield);
				}
				game.drawpickuptxt(window);
				pickup4.erase(pickup4.begin() + i);
				i--;
			}

		}
		if (forcefield == true)
			p1.drawforcefield(window, p);



		int h;
		h = c.size();

		if (chickenno >= 19)
		{
			beastresult = beast(window, game);
			
		}
		if (beastresult = 4)
			return 4;

		int t = game.getlives();
		if (t == 0)
		{
			return 0;
		}

		return 3;
	}

int level3::gameloop(sf::RenderWindow & window, game & game)
{
		handlevents(window);
		window.clear();
		result = wave1(window, game);

		window.display();

		if (result == 0)
		{
			return 0;
		}

		

		return 3;
	
		
		

}
