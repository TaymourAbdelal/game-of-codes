//
//  level2.hpp
//  Chicken Invaders
//
//  Created by Nada Ahmed Farag on 5/4/17.
//  Copyright © 2017 Nada Ahmed Farag. All rights reserved.
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include <vector>
#include "player.h"
#include "shots.h"
#include "rocks.h"
#include <time.h>
#include <iostream>
#include <string>
#include "game.h"
#include "pickups.h"
#ifndef level2_hpp
#define level2_hpp
class level2
{
private:
public:
	int boss;
	int beasthits = 0;
    sf::Text wavetxt;
    sf::Font font;
    player p;
	int result;
	
    sf::Clock c,clockExplosion ,clocktxt , clocktxt2;
    sf::Time t;
	int Numshots;
	int power = 1;
    std::vector<shots> container; //for bullets
    std::vector<rocks> rockcontainer;
    bool firing = false;
    bool playerflag = false;
    std::vector<int> flagrocks;
    int indexrocks[200]={0};
    /*sf::RenderWindow window;*/
    bool flagbullets[400];
    rocks r;
    int counter = 0;
	//------------------------------------General declaration (for pickups)
	int z;
	int rockcounter = 0;
	bool alive = true;
	bool drop3 = false, drop4 = false, drop2 = false, drop1 = false;
	std::vector<pickups> pickup4, pickup3, pickup2, pickup1;
	pickups p1, p2, p3, p4;
	int sc;
	sf::Clock clock, drop2c, drop1c, drop3c, drop4c, wavetxtc;
	bool forcefield = false;
	int j=0;


	
	
	sf::Clock clockspace, clockmove, clockspace2;
	bool flagspace = true, flagspaceup = true, flag = true;
	chicken spaceshipb;
	egg eggs;
	egg laser;
	std::vector<egg> laser1, laser2, egg2, egg1, egg3, egg4;
	/*sf::RenderWindow window;*/
	


//------------------------
    level2();
	int gameloop(sf::RenderWindow&  , class game & );
    int wave1(sf::RenderWindow&  , class game & );
	int beast2(sf::RenderWindow&, game & );
    


};

#endif /* level2_hpp */
