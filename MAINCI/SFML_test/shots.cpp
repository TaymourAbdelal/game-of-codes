#include <SFML/graphics.hpp>
#include "player.h"
#include "shots.h"
shots::shots()
{
	rect.scale(sf::Vector2f(0.1, 0.1));
	rect.setFillColor(sf::Color::Red);
	rect1.scale(sf::Vector2f(0.1, 0.1));
	rect1.setFillColor(sf::Color::Red);
}
void shots::shotscolorblue()
{
	rect.setFillColor(sf::Color::Blue);
}
shots::shots(sf::Vector2f size)
{
    rect.setSize(size);
    rect.setFillColor(sf::Color::Red);
	rect1.setSize(size);
	rect1.setFillColor(sf::Color::Red);
}
//------------------------
void shots::fire (float speed)
{
    rect.move(0,-speed);
	rect1.move(0, -speed);
}
//------------------------
/*int shots::getrightpos()
{
    return rect.getposition().x + rect.getSize().x;
}
//-------------------------
int shots::getleftpost()
{
    return rect.getposition().x;
}
//------------------------
int shots::gettoppos()
{
    return rect.getposition().y;
}
//-------------------------
int shots::getbottompost()
{
    return rect.getposition().y + rect.getSize().y;
}
//-------------------------------- */
void shots::setshotposition(sf::Vector2f x)
{
    rect.setPosition(sf::Vector2f (x));
	rect1.setPosition(x.x + 10, x.y);
}
//-----------------------------------
void shots::drawshots(sf::RenderWindow &window,int numberShots)
{
	if(numberShots==1)
    window.draw(rect);
	else if (numberShots ==2)
	{
		window.draw(rect);
		window.draw(rect1);
	}
	//rect.setPosition(sf::Vector2f(rect.getPosition().x + 20, rect.getPosition().y));
	//window.draw(rect);
}
