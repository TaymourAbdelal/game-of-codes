#include <SFML/graphics.hpp>
#include <SFML/Audio.hpp>
#ifndef player_h
#define player_h

class player
{
private:
	sf::RectangleShape rect;
	sf::Texture playertexture;
	sf::Texture extremeright;
	sf::Texture extemeleft;
	sf::Clock clock;
	float size;
	float positionx;
	float positiony;
	float position;
	sf::Texture texture1;
	sf::Texture texture2;
	sf::Texture texture3;
	sf::Texture texture4;
	sf::Texture texture5;
	sf::Texture texture6;
	sf::Texture texture7;
	sf::Texture texture8;
	sf::Texture texture9;
	sf::Sprite explosionsprite;
	sf:: Music blast;
public:
	sf::Sprite playersprite;
	/*void setsize(sf::Vector2f s);*/
	player();
	player(sf::Texture T, std::string string);
	void drawplayer(sf::RenderWindow &window, bool b, sf::Clock clockExplosion);
	float getsize();
	//void setsize(sf::Vector2f m);
	//void setposition();
	float getpositionx() const;
	float getpositiony() const;
	void movement(sf::Event &event, sf::RenderWindow &window);
	
	void explosion(sf::RenderWindow &window, sf::Clock);
};
#endif /* player_h */
