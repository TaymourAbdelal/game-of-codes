#pragma once
#ifndef pickups_h
#define pickups_h
#include "player.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include < iostream>
class pickups
{
private:
	sf::Texture pp1texture, pp2texture, pp3texture, pp4texture, forcefieldtexture;
	

public:
	sf::Sprite pp1, pp2, pp3, pp4, forcefield;
	pickups();
	int type;
	pickups(const pickups & p)
	{
		//this->pic = p.pic;
		this->pp4 = p.pp4;
		this->type = p.type;
	}
	void setpositionff(float , float);
	void drawforcefield(sf::RenderWindow &  , player&);
	void droppp1(float, float, sf::RenderWindow &);
	void droppp2(float, float, sf::RenderWindow &);
	void droppp3(float, float, sf::RenderWindow &);
	void droppp4(float, float , sf:: RenderWindow &);
	void drawpp1(sf::RenderWindow &);
	void drawpp2(sf::RenderWindow &);
	void drawpp3(sf::RenderWindow &);
	void drawpp4(sf::RenderWindow &);
	void movepp4();
	void movepp3();
	void movepp2();
	void movepp1();
	~pickups()
	{
		//delete pic;
	}


	
};
#endif