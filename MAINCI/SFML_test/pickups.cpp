#include "pickups.h"
#include < SFML/Graphics.hpp>
#include "game.h"
pickups::pickups()
{
	
	pp1texture.loadFromFile("shield.png");
	pp1.setTexture(pp1texture);
	pp1.setScale(0.05, 0.05);

	pp2texture.loadFromFile("redgun.png");
	pp2.setTexture(pp2texture);
	pp2.setScale(0.1, 0.1);

	pp3texture.loadFromFile("lightsaber.png");
	pp3.setTexture(pp3texture);
	pp3.setScale(0.05, 0.08);

	pp4texture.loadFromFile("17896081.png");
	pp4.setTexture(pp4texture);
	pp4.setScale(0.15, 0.15);

	forcefieldtexture.loadFromFile("forcefield.png");
	forcefield.setTexture(forcefieldtexture);


}

void pickups::droppp1(float x, float y, sf::RenderWindow & window)
{
	pp1.setPosition(sf::Vector2f(x, y));

}
void pickups::droppp2(float x, float y, sf::RenderWindow & window)
{
	pp2.setPosition(sf::Vector2f(x, y));

}
void pickups::droppp3(float x, float y, sf::RenderWindow & window)
{
	pp3.setPosition(sf::Vector2f(x, y));

}
void pickups::droppp4(float x, float y , sf::RenderWindow & window)
{
	
	int ranD = rand() % 3;
	if (ranD == 0)
	{
		sf::Texture * pic1 = new Texture;
		pic1->loadFromFile("shield.png");
		pp4.setTexture(*pic1);
		type = 0;
	}
	else if (ranD == 1)
	{
		Texture * pic1 = new Texture;
		pic1->loadFromFile("redgun.png");
		pp4.setTexture(*pic1);
		type = 1;
	}
	/*else if (ranD == 2)
	{
		Texture * pic1 = new Texture;
		pic1->loadFromFile("lightsaber.png");
		pp4.setTexture(*pic1);
		type = 2;
	}*/
	else //if (ranD == 2)
	{
		Texture * pic1 = new Texture;
		pic1->loadFromFile("17896081.png");
		pp4.setTexture(*pic1);
		type = 2;
	}
	/*else
	{
		Texture * pic1 = new Texture;
		pic1->loadFromFile("forcefield.png");
		pp4.setTexture(*pic1);
		type = 4;
	}*/
	pp1.setScale(0.05, 0.05);
	pp4.setPosition(sf::Vector2f(x, y));
	
}

void pickups::drawpp1(sf::RenderWindow & window)
{
	window.draw(pp1);

}
void pickups::drawpp2(sf::RenderWindow & window)
{
	
	window.draw(pp2);
	


}
void pickups::drawpp3(sf::RenderWindow & window)
{
	window.draw(pp3);

}
void pickups::drawpp4(sf::RenderWindow & window)
{
	
	window.draw(pp4);
	

}
void pickups::movepp4()
{
	pp4.move(0, 3);
}
void pickups::movepp3()
{
	pp3.move(0, 1);
}
void pickups::movepp2()
{
	pp2.move(0, 1);
}
void pickups::movepp1()
{
	pp1.move(0, 1);
}

void pickups::drawforcefield(sf::RenderWindow & window  , player & p)
{ 
	int x, y;
	forcefield.setPosition(sf::Vector2f(p.getpositionx()  - 100 , p.getpositiony()-100));
	window.draw(forcefield);
	
}
	/*else
	{
		x = g.getlives();
		x--;
		g.setlives(x);
	}
	*/
		
	

void pickups::setpositionff(float x, float y)
{
	forcefield.setPosition(sf::Vector2f(x, y));
}