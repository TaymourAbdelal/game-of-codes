//
//  eggs.cpp
//  chicken
//
//  Created by Hania's air on 4/28/17.
//  Copyright © 2017 Hania's air. All rights reserved.
//
#include<iostream>
#include "eggs.hpp"
#include "chicken.hpp"
using namespace sf;

egg:: egg()
{
	if (!beam.loadFromFile( "beama.png"))

    {

        std:: cout<< "Not loaded";

    }

    
    if (!eggtex.loadFromFile( "bluegg.png"))
    {
        std:: cout<< "Not loaded";
    }
    if (!eggtex2.loadFromFile( "eggcrack.png"))
    {
        std:: cout<< "Not loaded";
    }
    if (!eggtex3.loadFromFile( "grenade1.png"))
    {
        std:: cout<< "Not loaded";
    }
    if (!eggtex4.loadFromFile("knife.png"))
    {
        std:: cout<< "Not loaded";
    }
    if (!laser.loadFromFile( "beam.png"))
    {
        std:: cout<< "Not loaded";
    }
    eggs.setTexture(eggtex);
    eggtex.setSmooth(true);
    eggs.setScale(0.06, 0.06);
}

void egg::eggmovement(RenderWindow &window, float speed)
{
    eggs.move(0, speed);
}
void egg::seteggpos(float x, float y)
{
    eggs.setPosition(x,y);
    
}
void egg::drawegg(RenderWindow &window)
{
    window.draw(eggs);
}
Vector2f egg::geteggpos()
{
    return eggs.getPosition();
}
void egg::eggchange(RenderWindow &window)
{
    if (eggs.getPosition().y/2>=window.getSize().y)
    {
        eggs.setTexture(eggtex2);
        eggs.setScale(0.75, 0.75);
    }
    
}
void egg::grenadeload()
{
    eggs.setTexture(eggtex3);
    eggs.setScale(0.75, 0.75);
}
void egg::knifeload()
{
    eggs.setTexture(eggtex4);
    eggs.setScale(0.75, 0.75);
    
}
void egg::laserload()
{
    eggs.setTexture(laser);
    eggs.setScale(0.75, 0.75);
    
}
void egg::beamload()

{

	eggs.setTexture(beam);

	eggs.setScale(0.75, 0.75);


}