#include "game.h"
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include "chicken.hpp"
#include "eggs.hpp"
#include "shots.h"
#include "pickups.h"
using namespace std;

game::game()
{
	backgroundtexture.loadFromFile("background.jpg");
	background.setTexture(backgroundtexture);
	if (!font.loadFromFile("pdark.ttf"))
	{
		/*handleerror*/
	}
	/*sf::RenderWindow window(sf::VideoMode(800, 800), "Chicken Invaders");*/
	lives = "3";
	score = "0";
	rockets = "3";
	energy = "10";
	food = "9";



	scoretxt.setFont(font);
	scoretxt.setString("Score");
	scoretxt.setFillColor(sf::Color::White);
	scoretxt.setPosition(sf::Vector2f(0, 0));

	scorecount.setFont(font);
	scorecount.setString(score);
	scorecount.setFillColor(sf::Color::White);
	scorecount.setPosition(sf::Vector2f(130, 0));


	hud[0][0].setFont(font);
	hud[0][0].setFillColor(sf::Color::White);
	hud[0][0].setString(lives);
	hud[0][0].setPosition(sf::Vector2f(50, 750));

	hud[0][1].setFont(font);
	hud[0][1].setFillColor(sf::Color::White);
	hud[0][1].setString(rockets);
	hud[0][1].setPosition(sf::Vector2f(100, 750));

	hud[0][2].setFont(font);
	hud[0][2].setFillColor(sf::Color::White);
	hud[0][2].setString(energy);
	hud[0][2].setPosition(sf::Vector2f(150, 750));

	hud[0][3].setFont(font);
	hud[0][3].setFillColor(sf::Color::White);
	hud[0][3].setString(food);
	hud[0][3].setPosition(sf::Vector2f(220, 750));

	hudicons[0][0].loadFromFile("17896081.png");
	hudsprite[0][0].setTexture(hudicons[0][0]);

	hudsprite[0][0].scale(sf::Vector2f(0.15, 0.15));
	hudsprite[0][0].setPosition(0, 740);

	hudicons[0][1].loadFromFile("rocket.png");
	hudsprite[0][1].setTexture(hudicons[0][1]);
	hudsprite[0][1].scale(sf::Vector2f(0.3, 0.3));
	hudsprite[0][1].setPosition(60, 740);

	hudicons[0][2].loadFromFile("2000px-Emoji_u26a1.svg.png");
	hudsprite[0][2].setTexture(hudicons[0][2]);
	hudsprite[0][2].scale(sf::Vector2f(0.015, 0.015));
	hudsprite[0][2].setPosition(120, 755);

	hudicons[0][3].loadFromFile("115eaea48a28f7eddd8a45fbccae8e41_chicken-leg-clipart-fried-chicken-leg-clipart_757-580.png");
	hudsprite[0][3].setTexture(hudicons[0][3]);
	hudsprite[0][3].scale(sf::Vector2f(0.06, 0.06));
	hudsprite[0][3].setPosition(185, 750);

	hudbar.loadFromFile("bar.png");
	lowerhudbar.setTexture(hudbar);
	lowerhudbar.setPosition(0, 465);
	lowerhudbar.scale(sf::Vector2f(1.55, 1));

	


}
game::game(float height , float width , string x)
{
	backgroundtexture.loadFromFile("background.jpg");
	background.setTexture(backgroundtexture);
	if (!font.loadFromFile("pdark.ttf"))
	{
		/*handleerror*/
	}
	sf::RenderWindow window(sf::VideoMode(height, width), "Chicken Invaders");
	lives = "3";
	score = "0";
	rockets = "3";
	energy = "10";
	food = "9";
	//backgroundtexture.loadFromFile("background.jpg");
	//background.setTexture(backgroundtexture);
	//background.setPosition(sf::Vector2f(-100, 0));
	

	scoretxt.setFont(font);
	scoretxt.setString("Score");
	scoretxt.setFillColor(sf::Color::White);
	scoretxt.setPosition(sf::Vector2f(0,0));

	scorecount.setFont(font);
	scorecount.setString(score);
	scorecount.setFillColor(sf::Color::White);
	scorecount.setPosition(sf::Vector2f(130, 0));


	hud[0][0].setFont(font);
	hud[0][0].setFillColor(sf::Color::White);
	hud[0][0].setString(lives);
	hud[0][0].setPosition(sf::Vector2f(50, 750));

	hud[0][1].setFont(font);
	hud[0][1].setFillColor(sf::Color::White);
	hud[0][1].setString(rockets);
	hud[0][1].setPosition(sf::Vector2f(100, 750));

	hud[0][2].setFont(font);
	hud[0][2].setFillColor(sf::Color::White);
	hud[0][2].setString(energy);
	hud[0][2].setPosition(sf::Vector2f(150, 750));

	hud[0][3].setFont(font);
	hud[0][3].setFillColor(sf::Color::White);
	hud[0][3].setString(food);
	hud[0][3].setPosition(sf::Vector2f(220, 750));

	hudicons[0][0].loadFromFile("17896081.png");
	hudsprite[0][0].setTexture(hudicons[0][0]);
	
	hudsprite[0][0].scale(sf::Vector2f(0.15, 0.15));
	hudsprite[0][0].setPosition(0, 740);

	hudicons[0][1].loadFromFile("rocket.png");
	hudsprite[0][1].setTexture(hudicons[0][1]);
	hudsprite[0][1].scale(sf::Vector2f(0.3, 0.3));
	hudsprite[0][1].setPosition(60, 740);

	hudicons[0][2].loadFromFile("2000px-Emoji_u26a1.svg.png");
	hudsprite[0][2].setTexture(hudicons[0][2]);
	hudsprite[0][2].scale(sf::Vector2f(0.015, 0.015));
	hudsprite[0][2].setPosition(120, 755);

	hudicons[0][3].loadFromFile("115eaea48a28f7eddd8a45fbccae8e41_chicken-leg-clipart-fried-chicken-leg-clipart_757-580.png");
	hudsprite[0][3].setTexture(hudicons[0][3]);
	hudsprite[0][3].scale(sf::Vector2f(0.06, 0.06));
	hudsprite[0][3].setPosition(185, 750);

	hudbar.loadFromFile("bar.png");
	lowerhudbar.setTexture(hudbar);
	lowerhudbar.setPosition(0, 465);
	lowerhudbar.scale(sf::Vector2f(1.55, 1));

	wavetxt.setFont(font);
	wavetxt.setFillColor(sf::Color::White);
	wavetxt.setString(x);
	wavetxt.setPosition(sf::Vector2f(100, 400));


}
void game::drawhud(sf::RenderWindow &window)
{
	int i = 0;

	window.draw(scoretxt);
	window.draw(scorecount);
	window.draw(lowerhudbar);
	window.draw(wavetxt);
	for (int j = 0; j < 4; j++)
	{


		window.draw(hud[i][j]);

		window.draw(hudsprite[i][j]);
	}
	
	
}

void game::wave(sf::RenderWindow &window, string x, sf::Time t )
{
	wavetxt.setString(x);

	if ((t.asSeconds() >= 5.00000) && (t.asSeconds() <= 5.10000))
	{


	game:: waveclear();

	}
	
}
void game::setwavetxt(string x )
{
	wavetxt.setString(x);
	wavetxt.setFont(font);
	wavetxt.setFillColor(sf::Color::White);
	
	wavetxt.setPosition(sf::Vector2f(100, 400));


}
void game::waveclear()
{
	
	wavetxt.setString(" ");
	
}
void game::drawwavetxt(sf::RenderWindow& window)
{
	window.draw(wavetxt);
}
void game::setchickens(int x)
{
	chickens = x;
}
void game::killchicken()
{
	chickens = -1;

}
int game::getchickens()
{
	return chickens;
}

void game::drawbackground(sf::RenderWindow & window)
{
	
	
	background.setPosition(sf::Vector2f(-100, 0));
	window.draw(background);
}
//int game::setmusic(string x)
//{
//void music.openFromFile(x);
//	if (!music.openFromFile(x))
//	//handle
//	music.play();
//}
//int game::setbuttonssound(string x)
//{
//	if (!button.loadFromFile(x))
//		return -1;
//}
void game::setlives(int x)
{

	lives = std::to_string(x);
}
void game::drawlives(sf::RenderWindow & window)
{
		hud[0][0].setString(lives);
		window.draw(hud[0][0]);
}


void game::setscore(int x , sf::RenderWindow & window)
{
	score = std::to_string(x);
	
	scorecount.setString(score);
	window.draw(scorecount);

}
void game::drawscore(sf::RenderWindow &window)
{
	window.draw(scorecount);
}
void game::setrockets(int x  , sf::RenderWindow & window)
{
	std::to_string(x);
	rockets = x;
	hud[0][1].setString(rockets);
	window.draw(hud[0][1]);
}
void game::setenergy(int x , sf::RenderWindow & window)
{
	std::to_string(x);
	energy = x;
	hud[0][2].setString(energy);
	window.draw(hud[0][2]);
}
void game::setfood(int x, sf::RenderWindow & window)
{
	std::to_string(x);
	food = x;
	hud[0][3].setString(food);
	window.draw(hud[0][3]);
}
void game::setpickuptxt(std::string x)
{
	pickupstxt.setString(x);
	pickupstxt.setFont(font);
	pickupstxt.setFillColor(sf::Color::White);
	pickupstxt.setPosition(sf::Vector2f(200, 400));

		
}
void game::pickuptxtclear()
{
	pickupstxt.setString(" ");
}
void game::drawpickuptxt(sf::RenderWindow& window)
{
	window.draw(pickupstxt);
}

//int game::level2()
//{
//
//}
//int game:: level3()



//bool game::alive()
//{
//	if (lives == 0)
//		return false;
//	else
//		return true;
//}
//void game::playmusic()
//{
//	music.play();
//}
