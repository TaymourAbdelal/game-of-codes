#include "Darth.hpp"
#include "eggs.hpp"
#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>
Darth:: Darth()
{
    if (!DarthTexture.loadFromFile( "DarthVader1.png"))
    {
        std:: cout<< "Not loaded";
    }
    DarthSprite.setScale(1, 1);
  
    if (!DarthTexture1.loadFromFile( "DD.png"))
    {
        std:: cout<< "Not loaded";
    }
    DarthSprite.setScale(1, 1);
    DarthSprite.setTexture(DarthTexture);
    DarthSprite.setPosition(200,-300);
   beamsprite.setTexture(beam);
   beamsprite.setPosition(DarthSprite.getPosition().x,
	   DarthSprite.getPosition().y);
    glowballT.loadFromFile( "red.png");
	glowballS.setPosition(200,-300);
    glowballS.setTexture(glowballT);
   
	
}
//---------------------------------------
void Darth::drawVader(sf::RenderWindow &window)
{
    
    window.draw(beamsprite);
    window.draw(DarthSprite);
    window.draw(glowballS);
}
//----------------------------------------
void Darth::moveVader(sf::RenderWindow &window,sf::Clock flap)
{

    sf::Time done;
    done+= flap.restart();
    
    while (done> sf::seconds(0.5))
        done-=sf::seconds(0.5);
    if (done<=sf::seconds(0.25))
    {
        
        DarthSprite.setTexture(DarthTexture);
    }
    else if (done <=sf::seconds(0.5))
    {
        
        DarthSprite.setTexture(DarthTexture1);
		DarthSprite.setScale(2, 2);
    }
    
    //window.draw(DarthSprite);
    
}
void Darth::movementrectangle(float &angle)
{
	
    glowballS.move(r*sin(angle),r*cos(angle));
	
}
//---------------------------------------
bool Darth::Move(float speed,sf::RenderWindow &window,double x)
{
    
    
    if (DarthSprite.getPosition().x + x < window.getSize().x)
    {
        DarthSprite.move(speed, 0);
       // glowballS.move(speed,0);
		glowballS.setPosition(DarthSprite.getPosition().x + 300, DarthSprite.getPosition().y);
        return true;
    }
    else {
        return false;
        }
    /*float a = 0.05;
    DarthSprite.move(darthradius*cos(a),darthradius*sin(a));*/
    
    

}
//---------------------------------------------
bool Darth::MoveLeft(float speed,sf::RenderWindow &window,double x)
{
    if (DarthSprite.getPosition().x - x >= 0)
    {
        DarthSprite.move(-speed, 0);
       // glowballS.move(-speed,0);
		glowballS.setPosition(DarthSprite.getPosition().x + 300, DarthSprite.getPosition().y);
        return false;
    }
    else {
        return true;
        
        }
    
    
}
//------------------------------------------------
Vector2f Darth::getpos() const
{
    return DarthSprite.getPosition();
}
//-------------------------------------
float Darth::getxsize() const
{
    return DarthTexture.getSize().x;
}
//-------------------------------------
float Darth::getysize() const
{
    return DarthTexture.getSize().y;
}
//------------------------------------
void Darth::setpos(float x,float y)
{
    DarthSprite.setPosition(x, y);
	//glowballS.setPosition(DarthSprite.getPosition().x +300, DarthSprite.getPosition().y);
}
//----------------------------------
bool Darth::Movedown(int speed, RenderWindow &window, double x)
{
	if (DarthSprite.getPosition().y<window.getPosition().y - x)
	{
		DarthSprite.move(0, speed);
		glowballS.setPosition(DarthSprite.getPosition().x + 300, DarthSprite.getPosition().y);
		//glowballS.move(0, speed);
	}
		
	else
		return false;

	return true;

}
