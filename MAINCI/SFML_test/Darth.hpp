//
//  DarthVader.hpp
//  Chicken Invaders
//
//  Created by Nada Ahmed Farag on 5/4/17.
//  Copyright © 2017 Nada Ahmed Farag. All rights reserved.
//

#ifndef Darth_hpp
#define Darth_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "eggs.hpp"
class Darth
{
    public:
    sf::RectangleShape Darthrect;
    sf::Sprite glowballS;
    sf::Texture glowballT;
    float originalposition1 = 400;
    float originalposition2 = 400;
    int r = 5;
    //int darthradius = 10;
    sf::Texture beam;
    sf::Sprite beamsprite;
    sf::Texture DarthTexture;
    sf::Texture DarthTexture1;
    sf::Sprite DarthSprite;
    Darth();
    void moveVader(sf::RenderWindow &window,sf::Clock flap);
    void drawVader(sf::RenderWindow &window);
    void movementrectangle(float &angle);
   bool Move(float speed,sf::RenderWindow &window,double x);
    bool MoveLeft(float speed,sf::RenderWindow &window,double x);
    sf::Vector2f getpos() const;
    float getxsize() const;
    float getysize() const;
    void setpos(float x,float y);
	bool Movedown(int, sf::RenderWindow&, double);
    
};
#endif
