#pragma once
#ifndef level1_h
#define level1_h
#include "eggs.hpp"
#include "chicken.hpp"
#include "game.h"
#include "rocks.h"
#include "pickups.h"
#include "shots.h"
#include <iostream>
const int rows = 4;
const int cols = 3;
class level1
{
public:
	int j;
	int beasthits = 0;
	bool beastflag = false;
	int power = 1;
	int chickenkills = 0;
	bool playerflag = false;
	int indexeggs[200] = { 0 };
	int indexrocks[200] = { 0 };
	bool flagrocks[200];
	bool flageggs[200];
	bool forcefield = false;

	bool flagchicken[rows][cols];


	bool drop3 = false, drop4 = false, drop2 = false, drop1 = false;

	bool flag = true, drop = true;
	bool flagshot[200];
	pickups p1, p2, p3, p4;
	int sc;
	int chickenskilled = 0;
	chicken c[rows][cols];
	float x;
	float y;
	sf::Text wavetxt;
	rocks ro;
	egg e;
	float px, py;
	sf::Clock clock, drop2c, drop1c, drop3c, drop4c, wavetxtc, clockExplosion, clockchick, clocktxt, clocktxt2;
	/*sf::RenderWindow window;*/

	std::vector<egg> r;
	std::vector<shots> container;
	std::vector<rocks> rockcontainer;
	std::vector<pickups> pickup4, pickup3, pickup2, pickup1;
	//const int cols = 4;
	//const int rows = 2;
	int chickens = rows*cols;
	bool firing;
	int z;
	int result = 1000;
	int ends = 1;
	bool  alive = true;
	sf::Clock flap, clock1, clockdrop, rocksc , clocktxt7;
	player p;
	int Numshots;
	sf::Clock clockbeast, clockbeastmove, clockbeastdown;
	bool flagbeast = true, down = true;
	chicken beast;
	egg grenade, knife;
	std::vector<egg> grenadecont, grenadecont2;

	//std::vector<egg> r;
	//std::vector<shots> container;
	//std::vector<rocks> rockcontainer;
	//std::vector<pickups> pickup4, pickup3, pickup2, pickup1;

	//int chickens = rows*cols;
	//bool firing;
	//int z;
	//bool  alive = true;

	//sf::Clock flap, clock1, clockdrop, rocksc;
	//sf::Clock  drop2c, drop1c, drop3c, drop4c, wavetxtc, clockExplosion, clockc, clockchick;
	//player p;
	//bool playerflag = false;
	//int indexeggs[200] = { 0 };
	//int indexrocks[200] = { 0 };
	//bool flagrocks[200];
	//bool flageggs[200];
	//bool drop3 = false, drop4 = false, drop2 = false, drop1 = false;
	//bool flagchicken[rows][cols];
	//bool flag = true, drop = true;
	//bool flagshot[200];
	////shots newshot(sf::Vector2f(5, 10));
	//pickups p1, p2, p3, p4;
	//sf::RenderWindow window;
	//game gameg;

	//chicken c[rows][cols];
	//float x = c[0][0].getpos().x;
	//float y = c[0][0].getpos().y;
	//rocks ro;
	//egg e;
	//float px, py;
	//-----------------------------------------------

	level1();
	int wave1(sf::RenderWindow&, game&);
	int beast1(sf::RenderWindow&, game&);
	void handleevents(sf::RenderWindow&);
	int gameloop(sf::RenderWindow&, game&);
};



#endif // !levels_h
