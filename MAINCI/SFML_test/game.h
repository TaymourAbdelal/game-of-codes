#pragma once
#ifndef game_h
#define game_h
#include <SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include <iostream>
#include "eggs.hpp"
#include "shots.h"
#include "chicken.hpp"
class game {

private:
	sf::Time elapsed;
	sf::Font font;
	int  chickens;
	std::string score, lives, energy, rockets, food;
	sf::Text scorecount, scoretxt, livestxt, hud[1][4];
	sf::Texture scoreicon, hudicons[1][4], backgroundtexture, hudbar;
	sf::Sprite hudsprite[1][4], scoreicons, background, lowerhudbar;
	sf::Window gamewindow;
	sf::Music music;
	sf::SoundBuffer button;
	sf::RenderWindow window;



public:
	
	sf::Text wavetxt, pickupstxt;
	game();
	game(float , float , std:: string);
	void wave(sf::RenderWindow &, std::string , sf:: Time);
	void waveclear();
	void setchickens(int);
	int getchickens();
	void killchicken();
	/*void setmusic(std::string);*/
	//int setbuttonssound(std::string);
	//void setbackground(sf::Texture);
	void drawbackground(sf::RenderWindow &);
	void setwavetxt(std::string );
	void drawwavetxt(sf::RenderWindow & window);
	void setlives(int );
	void drawlives(sf::RenderWindow &);
	void setscore(int , sf::RenderWindow &);
	void drawscore(sf::RenderWindow&);
	void setrockets(int , sf::RenderWindow &);
	void setenergy(int , sf::RenderWindow &);
	void setfood(int , sf::RenderWindow &);
	void setpickuptxt(std::string);
	void pickuptxtclear();
	//int level1(sf::RenderWindow &, std::vector<egg> & r,std:: vector <shots> & contianer, bool flageggs[200], bool flagchicken[8][4] , int indexeggs[200], bool);
	void drawpickuptxt(sf::RenderWindow&);
	//void setgame(float, float)
	//void handleevents();

	//void exit();
	//bool alive();

	//void createpickup();
	//void createpickup2();
	//void createpickup3();
	//void createpickup4();
	int getlives() { return atoi(lives.c_str()); };
	int getscore() { return atoi(score.c_str()); };
	int getrockets() { return atoi(rockets.c_str()); };
	int getenergy() { return atoi(energy.c_str()); };
	int getfood() { return atoi(food.c_str()); };

	void drawhud(sf::RenderWindow &);

	/*void playmusic();*/


};
#endif
#pragma once

