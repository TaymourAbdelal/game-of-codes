//
//  eggs.hpp
//  chicken
//
//  Created by Hania's air on 4/28/17.
//  Copyright © 2017 Hania's air. All rights reserved.
//

#ifndef eggs_hpp
#define eggs_hpp
#include "chicken.hpp"
#include <stdio.h>


class egg
{
private:
    Texture eggtex;
    Texture eggtex2;
    Texture eggtex3;
    Texture eggtex4;
    Texture laser;
	Texture beam;
    
public:
    Sprite eggs;
    egg();
    //egg(Vector2f);
    void eggmovement(RenderWindow &,float);
    void seteggpos(float, float);
    void drawegg(RenderWindow &);
    Vector2f geteggpos();
    void eggchange(RenderWindow &);
    void grenadeload();
    void knifeload();
    void laserload();
	void beamload();
};
#endif /* eggs_hpp */
