#include "level1.h"
#include "game.h"
#include "player.h"
#include "shots.h"
#include "rocks.h"
#include "eggs.hpp"
#include "chicken.hpp"
#include "rocks.h"
#include "pickups.h"
#include <SFML/Graphics.hpp>

level1::level1()
{

	Numshots = 1;
	x = c[0][0].getpos().x;
	y = c[0][0].getpos().y;
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			flagchicken[i][j] = false;

	for (int m = 0; m < rockcontainer.size(); m++)
	{
		flagrocks[m] = false;
	}
	for (int m = 0; m < r.size(); m++)
	{
		flageggs[m] = false;
	}
	for (int i = 0; i < container.size(); i++)
	{
		flagshot[i] = false;
	}

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			c[i][j].setpos(i * 140, j * 100);

	beast.beastload();
	beast.setpos(50, -300);
	grenade.grenadeload();
	knife.knifeload();
	grenade.seteggpos(beast.getpos().x + beast.getsize() / 2, beast.getpos().y);
}

int level1::wave1(sf::RenderWindow& window , game & game)
{
	game.drawbackground(window);

	//shots fired===================================================================
	if (firing == true)
	{
		float a, b, c;
		c = p.getsize() / 2.0;
		a = p.getpositionx();
		b = p.getpositiony();
		shots newshot(sf::Vector2f(5, 10));
		shots uppedshot(sf::Vector2f(5, 10));
		newshot.setshotposition(sf::Vector2f(a + c, b));
		
		container.push_back(newshot);
		firing = false;
	}
	//if (power == 1)
	{
		for (int i = 0; i < container.size(); i++)
		{
			container[i].drawshots(window,Numshots);
			container[i].fire(100);
		}
	}
	/*else if (power == 2)
	{
		for (int i = 0; i < container.size(); i++)
		{
			container[i].drawshots(window,Numshots);
			container[i].fire(50);
		}
	}
	*/
	

	//player draw===================================================================
	p.drawplayer(window, playerflag, clockExplosion);
	game.drawhud(window);
	

	//Drop extra life pickup4===============================================================
	


	//chicken movement=========================================================
	/*c.flapchicken(window, clock);*/
	
	if (clockchick.getElapsedTime().asSeconds() >= 0.25) {

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				c[i][j].movechicken(window, flap);
				
				
				if (flagchicken[i][j] == false) { ////////////////////////////
					if (flag == true)
						flag = c[i][j].Move(10, window, 120);

					else
						flag = c[i][j].MoveLeft(10, window, 50);				
				}
			}

		}
		clockchick.restart();
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (rand() % 2000 == 0)

			{
				e.seteggpos(c[i][j].getpos().x, c[i][j].getpos().y );
				r.push_back(e);
			}

			for (int i = 0; i < r.size(); i++)
			{
				r[i].eggmovement(window, 0.2);
				r[i].eggchange(window);

			}
		}
	}


	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (flagchicken[i][j] == false)
			{
				c[i][j].drawchicken(window);
			}
			else
				c[i][j].setpos(800, 800);
		}
	}
	//rocks ==========================================================================

	

	//egg drops===============================================================

	for (int i = 0; i<rows; i++)
	{
		for (int j = 0; j<cols; j++)
		{
			if (rand() % 500 == 0)

			{
				e.seteggpos(c[i][j].getpos().x + 100, c[i][j].getpos().y + 50);
				r.push_back(e);
			}

			for (int i = 0; i<r.size(); i++)
			{
				r[i].eggmovement(window, 0.4);

				r[i].eggchange(window);

			}
		}
	}
	for (int i = 0; i<r.size(); i++)
		r[i].drawegg(window);
	for (int i = 0; i < container.size(); i++)
	{
		for (int j = 0; j < r.size(); j++)
		{
			if (container[i].rect.getGlobalBounds().intersects(r[j].eggs.getGlobalBounds()))
			{

				
				flageggs[j] = true;
				indexeggs[j]++;
				
			//game.drawscore(window);
			}
		}
	}
	for (int j = 0; j<r.size(); j++)
	{
		if (flageggs[j] == false)
			r[j].drawegg(window);
		if (flageggs[j] == true && indexeggs[j]<2)
		{

			r[j].drawegg(window);
		}

	}
	//=======================================shots collisions --------------------------------
	for (int x = 0; x < container.size(); x++)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (container[x].rect.getGlobalBounds().intersects(c[i][j].chick.getGlobalBounds()))
				{
					
							if (rand() % 10 == 0)
							{
								p4.droppp4(c[i][j].getpos().x, c[i][j].getpos().y, window);
								pickup4.push_back(p4);
							}

							
						
					

					
					//std::cout << "!!!!!!!!!!!!!!" << std::endl;
					flagchicken[i][j] = true;
					
					//c[i][j].setpos(1000, 1000);
					sc = game.getscore();
					sc = sc + 100;
					
					//game.setscore(sc , window);
					container.erase(container.begin()+x);
					x--;
					chickenkills++;
					i = rows;
					j = cols;
					break;
					
				}

			}
		}
	}
	for (int i = 0; i < pickup4.size(); i++)
	{
		pickup4[i].movepp4();
	}
	if (drop4 == false)
	{
		for (int i = 0; i < pickup4.size(); i++)
		{
			pickup4[i].drawpp4(window);
		}
	}
	j = game.getlives();
	//==============chicken collisions =======================================
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{

			if (c[i][j].chick.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
			{
				forcefield = false;
				p.playersprite.setPosition(400, 600);
				playerflag = true;
				alive = false;

				//cout << z;
				if (Numshots >  1)
					Numshots--;
				
				if (j - 1 > 0)
				{
					j--;
					playerflag = false;

					alive = true;
					game.setlives(j);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					//r[g].erase();




				}
				else
				{
					game.setlives(0);
					game.drawlives(window);
					return 0;
				}
				
			}
			
		}
	}
	j = game.getlives();
	for (int g = 0; g < r.size(); g++)
	{
		if (r[g].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
		{
			playerflag = true;
			alive = false;

			forcefield = false;

			if (Numshots > 1)
				Numshots--;
			r.erase(r.begin() + g);
			g--;
			
			{
				if (j - 1 > 0)
				{
					j--;
					playerflag = false;

					alive = true;
					game.setlives(j);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					//r[g].erase();




				}
				else
				{
					game.setlives(0);
					game.drawlives(window);
					return 0;
				}

		}
			
		}
	}

	game.setscore(sc, window);
	
	//=================================================



	sf::Time elapsed1 = wavetxtc.getElapsedTime();


	//text that appears=======================================================

	//// collision drop 4===========================================================================
	int shield = 0;
	for (int i = 0; i < pickup4.size(); i++)
	{
		if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
		{
			//int x;
			//

			//drop4 = true;
			//
			if (pickup4[i].type == 2)
			{
				x = game.getlives();
				x++;
				game.setlives(x);
				game.drawlives(window);
				

				//
				//game.drawpickuptxt(window);
			}
			else if (pickup4[i].type == 1)
			{
				drop2 = true;
				game.setpickuptxt("PowerUp");
				power = 2;
				if (Numshots < 2)
					Numshots++;

					
						
				//game.drawpickuptxt(window);
			}
			else if (pickup4[i].type == 0)
			{
				drop1 = true;
				forcefield = true; 
				game.setpickuptxt("Shield");
				
				shield = game.getlives();
				shield = shield + 1;
				game.setlives(shield);
			}
			game.drawpickuptxt(window);
			pickup4.erase(pickup4.begin() + i);
			i--;
		}

	}
	if (forcefield == true)
		p1.drawforcefield(window, p);
	//

	//// collision drop 3===========================================================================
	//for (int i = 0; i < pickup3.size(); i++)
	//{
	//	if (pickup3[i].pp3.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
	//	{
	//		
	//		
	//		drop3 = true;
	//		game.setpickuptxt("Laser");


	//		
	//		
	//		game.drawpickuptxt(window);
	//	}

	//}
	//

	//// collision drop 2===========================================================================
	//for (int i = 0; i < pickup2.size(); i++)
	//{
	//	if (pickup2[i].pp2.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
	//	{
	//		
	//		
	//		drop2 = true;
	//		game.setpickuptxt("PowerUp");
	//		power = 2;
	//		if (Numshots < 2)
	//			Numshots++;

	//	
	//		
	//		game.drawpickuptxt(window);
	//	}

	//}
	//
	//// collision drop 1===========================================================================
	//int shield = 0;
	//for (int i = 0; i < pickup1.size(); i++)
	//{
	//	if (pickup1[i].pp1.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
	//	{
	//		
	//		//std::cout << "1";
	//		drop1 = true;
	//		forcefield = true; 
	//		game.setpickuptxt("Shield");
	//		
	//		
	//		
	//		
	//		game.drawpickuptxt(window);
	//		shield = game.getlives();
	//		shield = shield + 1;
	//		game.setlives(shield);

	//	}

	//}
	
	//if (drop1c.getElapsedTime().asSeconds() > 10)
	{
		//drop1 = false;
		//drop1c.restart();
	}
	int j = game.getscore();
	
	if (chickenkills == rows*cols )
	{
		clocktxt2.restart();
		result =  beast1(window, game);
		
	}
	if (result == 2)
	{
		
		return 2;
		
		
	}
	if (result == 0)
	{
		return 0;
	}
	

	return 1;
}
int level1::beast1(sf::RenderWindow& window , game & game)
{
	//sf::Clock clocktxt3;
	

		

		if (firing == true)
		{
			float a, b, c;
			c = p.getsize() / 2.0;
			a = p.getpositionx();
			b = p.getpositiony();
			shots newshot(sf::Vector2f(5, 10));
			shots uppedshot(sf::Vector2f(5, 10));
			newshot.setshotposition(sf::Vector2f(a + c, b));
			uppedshot.setshotposition(sf::Vector2f(a + c + 20, b));
			container.push_back(newshot);
			firing = false;

		}
		//if (power == 1)
		{


			for (int i = 0; i < container.size(); i++)
			{



				container[i].drawshots(window, Numshots);
				container[i].fire(50);



			}
		}
		p.drawplayer(window, playerflag, clockExplosion);


		game.drawhud(window);
		if (clockbeastdown.getElapsedTime().asSeconds() >= 0.25)
		{
			if (down == true)
				down = beast.Movedown(10, window, -50);
			beast.movebeast(window, clockbeastmove);
			if (flagbeast == true)
				flagbeast = beast.Move(4, window, 600);

			else

				flagbeast = beast.MoveLeft(4, window, 400);

			//clockbeast.restart();
			clockbeastdown.restart();

		}
		/*if (clockbeast.getElapsedTime().asSeconds() >= 0.25)
		{
			beast.movebeast(window, clockbeastmove);
			if (flagbeast == true)
				flagbeast = beast.Move(4, window, 600);

			else

				flagbeast = beast.MoveLeft(4, window, 400);

			clockbeast.restart();

		}*/
		/*if (rand() % 1000000000000 == 0)

		{
			grenade.seteggpos(beast.getpos().x, beast.getsize());
			grenadecont.push_back(grenade);
		}*/

		for (int i = 0; i < grenadecont.size(); i++)
		{
			grenadecont[i].eggmovement(window, 4);
			//grenadecont[i].eggchange(window);

		}
		if (rand() % 300 == 0)

		{
			knife.seteggpos(beast.getpos().x, beast.getpos().y);
			grenadecont.push_back(knife);
		}
		if (rand() % 300 == 0)

		{
			knife.seteggpos(beast.getpos().x+200, beast.getpos().y);
			grenadecont2.push_back(knife);
		}
		for (int i = 0; i < grenadecont.size(); i++)
		{
			grenadecont[i].eggmovement(window, 5);
			//grenadecont[i].eggchange(window);

		}
		for (int i = 0; i < grenadecont2.size(); i++)
		{
			grenadecont2[i].eggmovement(window, 5);
			//grenadecont[i].eggchange(window);

		}
		for (int i = 0; i < grenadecont.size(); i++)
			grenadecont[i].drawegg(window);
		for (int i = 0; i < grenadecont2.size(); i++)
			grenadecont2[i].drawegg(window);
		//game.getscore 
		
		for (int j = 0; j < container.size(); j++)

			if (container[j].rect.getGlobalBounds().intersects(beast.chick.getGlobalBounds()))
			{
				//std::cout << "yass";
				beasthits++;
				//std::cout << "BEASTHIT" << beasthits << std::endl;
				
				
				container.erase(container.begin() + j);
			



			}
		int x = game.getscore();
		if (beasthits < 10)
			beast.drawchicken(window);
		else
		{
			chickenkills++;
			beast.setpos(1000, 1000);
			x = x + 1000;
			game.setscore(x, window);
			
			return 2;
			

		}
		

		game.setwavetxt(" Get Knived");
		
		//std::cout << clocktxt3.getElapsedTime().asSeconds();

		//----------------------------------------------------------------
		z = game.getlives();
		for (int e = 0; e < grenadecont.size(); e++)
			if (grenadecont[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

				if (z - 1 > 0)
				{
					z= z - 1;
					playerflag = false;

					alive = true;
					game.setlives(z);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					grenadecont.erase(grenadecont.begin() + e);
					e--;




				}
				else
				{
					playerflag = true;
					game.setlives(0);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);

					return 0;
					
				}
		for (int e = 0; e < grenadecont2.size(); e++)
			if (grenadecont2[e].eggs.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))

				if (z - 1 > 0)
				{
					z = z - 1;
					playerflag = false;

					alive = true;
					game.setlives(z);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);
					grenadecont2.erase(grenadecont2.begin() + e);
					e--;




				}
				else
				{
					playerflag = true;
					game.setlives(0);
					game.drawlives(window);
					p.drawplayer(window, playerflag, clockExplosion);

					return 0;

				}
		j = game.getlives();
		//==============beast collisions =======================================
		

				if (beast.chick.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
				{
					forcefield = false;
					p.playersprite.setPosition(400, 600);
					playerflag = true;
					alive = false;

					//cout << z;
					if (Numshots >  1)
						Numshots--;

					if (j - 1 > 0)
					{
						j--;
						playerflag = false;

						alive = true;
						game.setlives(j);
						game.drawlives(window);
						p.drawplayer(window, playerflag, clockExplosion);
						//r[g].erase();




					}
					else
					{
						game.setlives(0);
						game.drawlives(window);
						return 0;
					}

				}

			
		//Drop extra life pickup4===============================================================



		if (rand() % 200 == 0)

		{





			p4.droppp4(beast.getpos().x, beast.getpos().y, window);







			pickup4.push_back(p4);

		}



		for (int i = 0; i < pickup4.size(); i++)

		{



			pickup4[i].movepp4();



		}











		if (drop4 == false)

		{



			for (int i = 0; i < pickup4.size(); i++)

			{

				pickup4[i].drawpp4(window);



			}

		}

		//pickup3========================================================




		//------------------------------------------------------------------

		//-----------------------------------------------------------------

		//// collision drop 4===========================================================================

		int shield = 0;
		for (int i = 0; i < pickup4.size(); i++)
		{
			if (pickup4[i].pp4.getGlobalBounds().intersects(p.playersprite.getGlobalBounds()))
			{
				//int x;
				//

				//drop4 = true;
				//
				if (pickup4[i].type == 2)
				{
					x = game.getlives();
					x++;
					game.setlives(x);
					game.drawlives(window);


					//
					//game.drawpickuptxt(window);
				}
				else if (pickup4[i].type == 1)
				{
					drop2 = true;
					game.setpickuptxt("PowerUp");
					power = 2;
					if (Numshots < 2)
						Numshots++;



					//game.drawpickuptxt(window);
				}
				else if (pickup4[i].type == 0)
				{
					drop1 = true;
					forcefield = true;
					game.setpickuptxt("Shield");

					shield = game.getlives();
					shield = shield + 1;
					game.setlives(shield);
				}
				game.drawpickuptxt(window);
				pickup4.erase(pickup4.begin() + i);
				i--;
			}

		}
		if (forcefield == true)
			p1.drawforcefield(window, p);



		
		if ((wavetxtc.getElapsedTime().asSeconds() >= 5) && (wavetxtc.getElapsedTime().asSeconds() <= 5.1))
			game.pickuptxtclear();
		
	
		
}


void level1::handleevents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
		{
			firing = true;

		}

		p.movement(event, window);
	}

}
int level1::gameloop(sf::RenderWindow &window , game & game)
{
	
	/*window.create(sf::VideoMode(800, 800), "Chicken Invaders");*/
	
		//
		//std::cout << "---------------------" << std::endl;
		/*for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++)
				std::cout << flagchicken[i][j];
			std::cout << std::endl;*/
		
		handleevents(window);
		window.clear();
		ends = wave1(window ,game);
		
		window.display();
		if (ends == 2)
			return 2;
	
		if (ends == 0)
			return 0;

		return 1;
	
		
	
	
	
}
		
	
	



