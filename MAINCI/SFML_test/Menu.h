#pragma once

#ifndef menu_h
#define menu_h
#include "game.h"
#include <String>
#include <SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<SFML/Window.hpp>
const int Max_Number_of_Items = 3 ;
class menu

{
private:
	





public:
	int l = 10;
	int actualsize, selecteditemindex = 0 , selecteditemindexem = 1;
	sf::Texture texture, buttonstexture[Max_Number_of_Items] , backgroundtxt , backgroundmenu  ;
	sf::RectangleShape buttonsmainmenu[Max_Number_of_Items];
	sf::Window firstmenu;
	sf::Window endscreen;
	sf::Text text[Max_Number_of_Items] , score[4] , victorytxt [3] , highscoremenu[5];
	sf::Font font;
	sf::Texture buttonsMain[100];
	sf::Music music;
	sf::SoundBuffer buttons;
	sf::Sprite background , backgroundmenusprite;
	/*menu();*/
	menu(float height, float width);
	/*endmenu();*/

	/*int setmusic(std::string);*/
	int getpressed() {return selecteditemindex;};

	/*int setbuttonsound(std::string);*/
	void moveup();
	void movedown();
	void setbackgroundmenu(sf::Window &window);
	/*void setbackgroundendscreen(sf::Texture);*/
	void drawmainmenu(sf::RenderWindow &window);
	int mainmenuloop(menu&, sf::RenderWindow& ,std::vector<int>);
	int handleevents(sf::RenderWindow & , menu &);
	int endscreenhandleevents(sf::RenderWindow &, menu &);
	void endmenu(sf::RenderWindow &, game &);
	void victorymenu(sf::RenderWindow &, game & );

	int endscreenloop(sf::RenderWindow &, game & , menu&);
	int victorymenuloop(sf::RenderWindow &, game &, menu&);
	void sethighscore(std::vector<int>);

	void drawhighscore(std::vector<int>);
	//void drawendscreen(sf::RenderWindow $);
	//void quit();
	//void playmusic();
	//void playbuttonsound();









};
#endif