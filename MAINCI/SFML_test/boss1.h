#pragma once
#ifdef boss1_h
#define boss1_h
#include "SFML/Graphics.hpp"
#include <iostream>


class boss1
{
private:
	sf::Texture eggstexture, bosstextrue; 
	sf::Sprite eggs, boss;

public:
	boss1();
};

