//
//  scoreboard.hpp
//  Chicken Invaders
//
//  Created by Nada Ahmed Farag on 5/8/17.
//  Copyright © 2017 Nada Ahmed Farag. All rights reserved.
//
#include <fstream>
#include <string>
#include <vector>
using namespace std;


#ifndef scoreboard_hpp
#define scoreboard_hpp

#include <stdio.h>
class scoreboard
{
public:
    //Variables
    fstream scoresFile;
   // ofstream inputfile;
    string filename = "scores.txt";
    int stringscore;
    
    scoreboard();
    void addScore(int score);
    vector<int> readexistingscores();
    vector<int> sort(vector<int> &);
    string hypotheticalnewscore = "12";
    void printnewscores(bool PermissionToErase);
};

#endif /* scoreboard_hpp */
