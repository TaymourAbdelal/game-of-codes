#include <SFML/graphics.hpp>
#include "shots.h"
#ifndef rocks_h
#define rocks_h
class rocks
{
private:
    int speed;
    sf::Vector2f direction;
    sf::Clock clock;
    
public:
    sf::Sprite sprite;
    sf::Texture texture;
    sf::Texture pooftexture;
    rocks();
    rocks(const rocks &r);
    void setposition(float &y);
    void scalerocks(float &x,float &y);
	int getposition();
    void draw(sf::RenderWindow &window);
    bool isrockstillthere();
    void move(sf::RenderWindow &window);
    void fillcontainer();
    void collision(shots s);
    void changetopooftexture(bool b);
    
    
    
};

#endif /* rocks_h */
