#include <SFML/graphics.hpp>
#include <SFML/System.hpp>

#include "rocks.h"
#include <cmath>
#include <cstdlib>
#include <iostream>
rocks::rocks()
{
    texture.loadFromFile("asteroidd.png");
    
    pooftexture.loadFromFile( "otherpoof.png");
    sprite.setTexture(texture);
    //float x = rand() % 8 -4;
    //float y = rand() % 8 -4;
    //sprite.setPosition(0, -y);
    
}
//--------------------------
rocks::rocks(const rocks &r)
{
    this->sprite = r.sprite;
    this->texture = r.texture;
    this-> pooftexture = r.pooftexture;
}//----------------------------
void rocks::setposition(float &y)
{
    sprite.setPosition(0, y);
}
//---------------------------
void rocks::scalerocks(float &x,float &y)
{
    sprite.scale(x, y);
}
//------------------------------
void rocks::draw(sf::RenderWindow &window)
{
    window.draw(sprite);
}
//---------------------------
bool rocks::isrockstillthere()
{   bool is_alive = true;
    return is_alive;
}
//---------------------------

void rocks::move(sf::RenderWindow &window)
{   speed = rand() % 100 + 20;
    if (sprite.getPosition().x < window.getSize().x + 100)
    {  
        sprite.move(speed,speed);
        float b = sprite.getPosition().y;
        float a = sprite.getPosition().x;
        sprite.setOrigin(texture.getSize().x/ 2,texture.getSize().y/2);
        //float rotation = ( atan2( b, a ) ) * 180 / 3.14;
//        sprite.setRotation(rotation + 180);
        
        sprite.rotate(50);

    }
    
}
//----------------------------------
void rocks::changetopooftexture(bool b)
{   if (b == false){
    sprite.setTexture(pooftexture);
    //sprite.setScale(1.5, 1.5);
}
}

int rocks::getposition()
{
	return sprite.getPosition().x;
}


 
