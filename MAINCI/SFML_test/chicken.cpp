//
//  chicken.cpp
//  chicken
//
//  Created by Hania's air on 4/8/17.
//  Copyright © 2017 Hania's air. All rights reserved.
//

#include "chicken.hpp"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>
#include <cmath>
using namespace sf;
chicken:: chicken()
{
    if (!texture1.loadFromFile("chick1.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.1, 0.1);
    
    if (!texture2.loadFromFile( "chick2.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.1, 0.1);
    
    if (!texture3.loadFromFile("chick3.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.1, 0.1);
    
    if (!texture4.loadFromFile("chick4.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.1, 0.1);
    
}

void chicken::drawchicken(RenderWindow &window)
{
    window.draw(chick);
}

void chicken::movechicken(RenderWindow &window,Clock flap)
{
    
    Time done;
    done+= flap.restart();
    
    while (done> seconds(1))
        done-=seconds(1);
    if (done<=seconds(0.25))
        
        chick.setTexture(texture1);
    
    else if (done<=seconds(0.5))
        
        chick.setTexture(texture2);
    
    else if (done<=seconds(0.75))
        
        chick.setTexture(texture3);
    
    else
        
        chick.setTexture(texture4);
    // drawchicken(window);
}
bool chicken::MoveLeft(int speed,RenderWindow &window,double x)
{
    if (chick.getPosition().x - x >= 0)
    {
        chick.move(-speed, 0);
        return false;
    }
    else {
        //chick.move(speed, 0);
        return true;
        
    }
    
    
}
bool chicken::Move(int speed,RenderWindow &window,double x)
{
    
    //if (chick.getPosition().x < 100)
    //chick.move(speed, 0);
    
    
    if (chick.getPosition().x + x < window.getSize().x)
    {
        chick.move(speed, 0);
        return true;
    }
    else {
        //chick.move(-speed, 0);
        return false;
    }
    
    
    
}


float chicken::getsize() const
{
    return texture1.getSize().x;
}

Vector2f chicken::getpos() const
{
    return chick.getPosition();
}
void chicken::setpos(float x, float y)
{
    chick.setPosition(x, y);
}
void chicken::beastload()
{
    if (!btex1.loadFromFile( "beast1up.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.75, 0.75);
    
    if (!btex2.loadFromFile("beast1mid.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.75, 0.75);
    
    if (!btex3.loadFromFile ("beast1down.png"))
    {
        std:: cout<< "Not loaded";
    }
    chick.setScale(0.75, 0.75);
}
void chicken::movebeast(RenderWindow &window, Clock clock)
{
    Time done;
    done+= clock.restart();
    
    while (done> seconds(0.4))
        done-=seconds(0.4);
    if (done<=seconds(0.1))
        
        chick.setTexture(btex1);
    
    else if (done<=seconds(0.2))
        
        chick.setTexture(btex2);
    
    else if (done<=seconds(0.3))
        
        chick.setTexture(btex3);
    
    else
        
        chick.setTexture(btex2);
    
}
void chicken::beast2load()
{
    if (!shiptex.loadFromFile(  "ship.png" ))
        std:: cout<< "Not loaded";
    
    chick.setScale(0.8, 0.8);
    
}
void chicken::beast2move()
{
    chick.setTexture(shiptex);
}
bool chicken::Moveup(int speed,RenderWindow &window, double x)
{
    if(chick.getPosition().y>window.getPosition().y)
        chick.move(0,-speed);
    else
        return false;
    
    return true;
    
}
bool chicken::Movedown(int speed,RenderWindow &window, double x)
{
    if(chick.getPosition().y<window.getPosition().y-x)
        chick.move(0,speed);
    else
        return false;
    
    return true;
    
}
