#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include "ResourcePath.hpp"
#include "chicken.hpp"
#include "eggs.hpp"
int const rows=4;
int const cols=2;
void render(sf::RenderWindow &window, chicken c, egg egg);

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 800), "SFML window");
    chicken c[rows][cols];
    chicken beast;
    beast.beastload();
    beast.setpos(50, 100);
    for (int i=0; i<rows; i++)
    
        for (int j=0;j<cols; j++)
        
            c[i][j].setpos(i*140, j*100);
    
    egg e, grenade,knife;
    grenade.grenadeload();
    knife.knifeload();
    float p=1.0;
    float x= c[0][0].getpos().x;
    float y= c[0][0].getpos().y;
    grenade.seteggpos(x+100, y+50);
    grenade.seteggpos(beast.getpos().x+beast.getsize()/2, beast.getpos().y);
    bool flag =true, flag2=true;
    sf::Clock clock;
    sf:: Clock flap,clock1,clock2,clock3,clocknife;
    std::vector<egg> r,grenadecont;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
            {
                window.close();
            }
        }
        if(clock1.getElapsedTime().asSeconds()>=0.25)
        for (int i=0; i<rows; i++)
        {
            for (int j=0;j<cols; j++)
            {
                c[i][j].movechicken(window,clock);
                
        
        
            if(flag==true)
            flag=c[i][j].Move(7,window,250);
            
            else
                
             flag=c[i][j].MoveLeft(7, window,250);
            
            clock1.restart();
        
            }
        }
        for (int i=0; i<rows; i++)
        {
            for (int j=0;j<cols; j++)
            {
                if (rand()%2500==0)
            
                {
                    e.seteggpos(c[i][j].getpos().x+100, c[i][j].getpos().y+50);
                    r.push_back(e);
                }
        
                for (int i=0; i<r.size(); i++)
                {
                    r[i].eggmovement(window, 0.2);
                    r[i].eggchange(window);
            
                }
            }
        }
        if(clock3.getElapsedTime().asSeconds()>=0.25)
        {
            beast.movebeast(window, clock2);
            if(flag2==true)
                flag2=beast.Move(4,window,600);
            
            else
                
                flag2=beast.MoveLeft(4, window,400);
            
            clock3.restart();
            
        }
        if (rand()%1000==0)
            
        {
            grenade.seteggpos(beast.getpos().x+200, beast.getpos().y+200);
            grenadecont.push_back(grenade);
        }
        
        for (int i=0; i<grenadecont.size(); i++)
        {
            grenadecont[i].eggmovement(window, 1);
            grenadecont[i].eggchange(window);
            
        }
        if (rand()%4000==0)
            
        {
            knife.seteggpos(beast.getpos().x+beast.getsize()/2, beast.getpos().y);
            grenadecont.push_back(knife);
        }
        
        for (int i=0; i<grenadecont.size(); i++)
        {
            grenadecont[i].eggmovement(window, 1);
            grenadecont[i].eggchange(window);
            
        }
       
        

        window.clear();
        for (int i=0; i<r.size(); i++)
            r[i].drawegg(window);
        for (int i=0; i<grenadecont.size(); i++)
            grenadecont[i].drawegg(window);
        for (int i=0; i<rows; i++)
            for (int j=0;j<cols; j++)
                c[i][j].drawchicken(window);
        beast.drawchicken(window);
        window.display();
        
    }
    
    
    
    return EXIT_SUCCESS;
}
//void render(sf::RenderWindow &window, chicken c, egg egg, egg r[])
//{
//    window.clear();
//    for (int i=0; i<r.size(); i++)
//    {
//        r[i].drawegg(window);
//        r[i].eggmovement(window, 8);
//    }
//    c.drawchicken(window);
//
//    window.display();
//}
